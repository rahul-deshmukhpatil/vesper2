import urllib.parse as urlparse
from time import sleep

# zerodha
from kiteconnect import KiteConnect
from selenium import webdriver
# chrome options class is used to manipulate various properties of Chrome driver
# find the above condition/conntent by the xpath, id etc.
from selenium.webdriver.common.by import By
# finds that content
from selenium.webdriver.support import expected_conditions as EC
# waits till the content loads
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


class TestZerodhaChromeDriver:
    def __init__(self):
        # credentials
        self.api_key = 'yabfgbz64qlv3caj'
        self.api_secret = 'uzf6ae99a9qikgrba7spwejb8g67fgcj'
        self.account_username = 'DG2081'
        self.account_password = '1Jun2019'
        self.account_two_fa = int(986008)
        self.kite = KiteConnect(api_key=self.api_key)

        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        # driver = webdriver.Chrome(
        #    executable_path='/Users/entirety/Desktop/chromedriver', options=options)

    def login(self):
        self.login_username_password()
        self.login_2fa()
        self.get_request_token()

    def login_username_password(self):
        self.driver.get(self.kite.login_url())

        # //tagname[@attribute='value']
        # tagname = div,

        # identify login section
        form = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.XPATH, '//div[@class="login-form"]')))

        # enter the ID
        self.driver.find_element_by_xpath("//input[@type='text']").send_keys(
            self.account_username)

        # enter the password
        self.driver.find_element_by_xpath(
            "//input[@type='password']").send_keys(
            self.account_password)

        # submit
        self.driver.find_element_by_xpath("//button[@type='submit']").click()

    def login_2fa(self):
        # sleep for a second so that the page can submit and proceed to upcoming question (2fa)
        sleep(1)
        form = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.XPATH, '//div[@class="login-form"]//form')))

        # identify login section for 2fa
        # enter the 2fa code
        self.driver.find_element_by_xpath(
            "//input[@type='password']").send_keys(
            self.account_two_fa)

        # submit
        self.driver.find_element_by_xpath("//button[@type='submit']").click()
        sleep(1)
        self.current_url = self.driver.current_url

        self.driver.close()

    def get_request_token(self):
        parsed = urlparse.urlparse(self.current_url)
        request_token = urlparse.parse_qs(parsed.query)['request_token'][0]

        access_token = \
            self.kite.generate_session(request_token=request_token,
                                       api_secret=self.api_secret)[
                'access_token']

        self.kite.set_access_token(access_token)

        response = self.kite.ltp('MCX:CRUDEOIL22FEBFUT')

        print(f'response ltp : {response}')


driver = TestZerodhaChromeDriver()
driver.login()
