import pandas as pd
from ratelimit import limits, sleep_and_retry

from vesper.connectors.common.zerodha.zerodha_login import ZerodhaLogin
from vesper.connectors.histmktdata.api import Intervals
from vesper.connectors.histmktdata.zerodha.api import ZerodhaHistMktdataApi
from vesper.utils.file_utils import write_as_hdf5
from vesper.utils.loggers import logger


class HistoricalDownload(ZerodhaLogin):
    ONE_SECOND = 1

    def __init__(self):
        ZerodhaLogin.__init__(self)
        ZerodhaLogin.setup(self)

    @sleep_and_retry
    @limits(calls=3, period=ONE_SECOND)
    def download(self, *args, **kwargs):
        try:
            return self.kiteconnect.historical_data(*args, **kwargs)
        except Exception as ex:
            logger.error(f'Exception occurred : {ex}')
            return ex

    def close(self):
        ZerodhaLogin.close(self)


hd = HistoricalDownload()

from_date = '2012-01-16 00:00:00'
to_date = '2022-02-18 00:00:00'
intervals = ['minute', '3minute', '5minute', '10minute', '15minute',
             '30minute', '60minute', 'day']

from_date = '20160216'
to_date = '20220223'
intervals = ['1m', '3m', '5m', '10m', '15m',
             '30m', '60m', 'day']

data = {}
instrument_token = '13308674'
instrument_token = '275855110'
instrument_token = '444163'
instrument_token = '586755'  # USDINR22429FUT
filename = '/home/rahul/bitbucket/test/sample.h5'
for interval in Intervals:
    # instrument_token = input('new instrument_token : ') or instrument_token

    norm_interval, fd, td = ZerodhaHistMktdataApi.get_normalized_interval_date(
        interval,
        from_date,
        to_date,
        is_fno=True)
    instr_data = hd.download(instrument_token=instrument_token,
                             from_date=fd,
                             to_date=td,
                             interval=norm_interval,
                             continuous=0, oi=1)

    logger.info(f'downloaded {instrument_token} : {norm_interval}')
    instr_data = pd.DataFrame(instr_data)
    data[norm_interval] = instr_data
    write_as_hdf5(instr_data, filename,
                  f'data/ohlc/{interval}')

read_data = {}

for interval in intervals:
    read_df = pd.read_hdf(filename, key=f'data/ohlc/{interval}')
    read_data[interval] = read_df

assert read_data == data
hd.close()
