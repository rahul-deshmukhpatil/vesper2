import pandas as pd

from vesper.connectors.histmktdata.api import Intervals

data = {}
for interval in Intervals:
    df = pd.read_hdf('~/bitbucket/test/db/20220223/mktdata/zerodha/BSE-ALKEM'
                     '-20220223.h5',
                     key=f'/data/ohlc/'
                     f'{interval.value}')
    data[interval.value] = df
pass
