import json

from vesper.utils import jsonutils
from vesper.utils.constants import Broker
from vesper.utils.kafka_prod_cons import KafkaMessanger


def run():
    while True:
        messages = km.get_kafka_messages()
        for message in messages:
            json_str = json.dumps(
                message.value, default=jsonutils.custom_datetime_encoder) \
                + '\n'


km = KafkaMessanger(cons_topics=[Broker.INVALID_BROKER])
run()
