import json
import urllib
import webbrowser
from urllib.parse import parse_qs, urlparse

import requests
from fyers_api import accessToken
from fyers_api import fyersModel
from fyers_api.Websocket import constants, ws
from fyers_api.Websocket.ws import FyersSocket

client_id = 'LM1XTRVC2Q-100'
secret_key = 'XWNHS8C906'
redirect_uri = 'https://127.0.0.1:5000'
response_type = 'code'
grant_type = 'authorization_code'

state = 'optional_state'
scope = 'The value in scope must be openid if being passed. Though this is' \
        ' an optional field '
Nonce = 'The value in nonce can be any random string value. This is also an ' \
        'optional field '

"""Create session"""
session = accessToken.SessionModel(client_id=client_id,
                                   secret_key=secret_key,
                                   redirect_uri=redirect_uri,
                                   grant_type=grant_type,
                                   response_type=response_type)

print(f'created session = {session}')
response = session.generate_authcode()

print(f'session generate_authcode response = {response}')

automated_auth = True
auth_code = None
if automated_auth:
    # automated auth_code token
    pan = 'BRGPP8831A'
    password = 'Rahu@9760'
    username = 'XG07489'
    pin = int('9860')
    redirect_uri = "http://127.0.0.1/loginstatus"

    data1 = f'{{"fy_id":"{username}","password":"{password}","app_id":"2",' \
            f'"imei":"","recaptcha_token":""}} '
    requests_session = requests.Session()
    r1 = requests_session.post(
        'https://api.fyers.in/vagator/v1/login', data=data1)
    request_key = r1.json()["request_key"]

    data2 = f'{{"request_key":"{request_key}","identity_type":"pin",' \
            f'"identifier":"{pin}","recaptcha_token":""}} '
    r2 = requests_session.post(
        'https://api.fyers.in/vagator/v1/verify_pin', data=data2)
    headers = {
        'authorization': f"Bearer {r2.json()['data']['access_token']}",
        'content-type': 'application/json; charset=UTF-8'
    }

    data3 = f'{{"fyers_id":"{username}","app_id":"{client_id[:-4]}",' \
            f'"redirect_uri":"{redirect_uri}","appType":"100",' \
            f'"code_challenge":"","state":"abcdefg","scope":"","nonce":"",' \
            f'"response_type":"code","create_cookie":true}} '
    r3 = requests_session.post(
        'https://api.fyers.in/api/v2/token', headers=headers, data=data3)
    parsed = urlparse(r3.json()['Url'])
    auth_code = parse_qs(parsed.query)['auth_code'][0]
else:
    webbrowser.open(response, new=2)
    auth_code_url = input('input auth_code from web browser : ')
    url_parts = auth_code_url.split('&')
    auth_code = None
    AUTH_CODE = 'auth_code='
    for part in url_parts:
        if part.startswith(AUTH_CODE):
            auth_code = part[len(AUTH_CODE):]

session.set_token(auth_code)
response = session.generate_token()
print(f'session generate_token response = {response}')
access_token = response["access_token"]

fyers = fyersModel.FyersModel(client_id=client_id, token=access_token,
                              log_path="/home/rahul/bitbucket/test/fyers-logs/")

print(f'generated fyersModel:{fyers}')
# (for symbol websocket the data_type has to be “symbolData”)
data_type = 'symbolData'


# (This is the custom message function which we need to have in order to
# receive the Symbol / order update and accordingly you can manipulate the
# data you receive through this function)
def custom_message(msg):
    print(f"Custom message :{msg}")


def custom_message_static(msg):
    print(f"Custom message static :{msg}")


class TestMktData:
    @staticmethod
    def custom_message_static(msg):
        print(f"TestMktData Custom message static :{msg}")

    def custom_message(self, msg):
        print(f"TestMktData Custom message :{msg}")


ws.websocket_data = custom_message
# (by default) If you don’t want to get the orderUpdate over the console you
# can set the run_background to True
APP_ALIAS_CLIENT_ID = 'LM1XTRVC2Q-100'
combined_access_token = f'{APP_ALIAS_CLIENT_ID}:{access_token}'


class CustomFyersSocket(FyersSocket):
    def __init__(self, *args, **kwargs):
        FyersSocket.__init__(self, *args, **kwargs)

    def _FyersSocket__on_message(self, ws, msg):
        if self._FyersSocket__data_type == "symbolData":
            self.response = self.parse_symbol_data(msg)
            if type(msg) == str:
                if "error" in msg:
                    msg = json.loads(msg)
                    self.response_out["s"] = msg["s"]
                    self.response_out["code"] = msg["code"]
                    self.response_out["message"] = msg["message"]
                    self.response = self.response_out
                    self.logger.error(self.response)
                    if self.websocket_data is not None:
                        self.websocket_data(self.response)
                    else:
                        print(f"Response:{self.response}")
            else:
                if self.websocket_data is not None:
                    self.websocket_data(self.response)
                else:
                    print(f"Response:{self.response}")
            # self.logger.debug(self.response)
        else:
            self.response = msg
            if type(msg) == str:
                if "error" in msg:
                    msg = json.loads(msg)
                    self.response_out["s"] = msg["s"]
                    self.response_out["code"] = msg["code"]
                    self.response_out["message"] = msg["message"]
                    self.response = self.response_out
                    self.logger.error(self.response)
                    if self.websocket_data is not None:
                        self.websocket_data(self.response)
                    else:
                        print(f"Response:{self.response}")
                else:
                    if self.websocket_data is not None:
                        self.websocket_data(self.response)
                    else:
                        print(f"Response:{self.response}")
            else:
                if self.websocket_data is not None:
                    self.websocket_data(self.response)
                else:
                    print(f"Response:{self.response}")
        # self.websocket_data()
        return

    def _FyersSocket__construct_send_message(self, symbol="",
                                             unsubscribe_flag=False):
        if unsubscribe_flag == True:
            SUB_T = 0
        else:
            SUB_T = 1
        if self._FyersSocket__data_type == constants.KEY_ORDER_UPDATE:
            message = {"T": "SUB_ORD", "SLIST": [], "SUB_T": SUB_T}
            message["SLIST"] = constants.KEY_ORDER_UPDATE
        elif self._FyersSocket__data_type == constants.KEY_DATA_UPDATE:
            message = {"T": "SUB_L2", "L2LIST": [], "SUB_T": SUB_T}
            message["L2LIST"] = symbol
            symbols = symbol
            symbols_dict = {"symbols": ""}
            symbols_dict["symbols"] = ','.join(symbols)
            url_params = urllib.parse.urlencode(symbols_dict)
            url = self.data_url + "?" + url_params
            # url = urllib.parse.unquote(url, encoding="utf-8")
            get_quotes = requests.get(url=url,
                                      headers={
                                          "Authorization": self._FyersSocket__access_token,
                                          'Content-Type': self.content})
            quotes_response = get_quotes.json()
            if quotes_response["s"] == "error":
                self.response_out["s"] = quotes_response["s"]
                self.response_out["code"] = quotes_response["code"]
                self.response_out["message"] = quotes_response["message"]
                return self.response_out
            for symbol in quotes_response["d"]:
                symbol_data = symbol["v"]["symbol"]
                fy_token = symbol["v"]["fyToken"]
                data_dict = {"fy_token": fy_token, "symbol": symbol_data}
                self.get_symtoken.append(data_dict)
        # message["SUB_T"] = int(subscribe)
        return json.dumps(message)


# if self.__ws_message is not None:
#     self.__ws_message(msg)


run_background = True
fyersSocket = CustomFyersSocket(access_token=combined_access_token,
                                run_background=run_background,
                                log_path='/home/rahul/bitbucket/test/fyers-logs/')

# fyersSocket.background_flag = False
# fyersSocket.websocket_data = custom_message_static
test_obj = TestMktData()
fyersSocket.websocket_data = test_obj.custom_message
print(f'generated fyersSocket {fyersSocket}')

symbol = ['NSE:SBIN-EQ']
symbol = ['NSE:L&TFH-EQ']
symbol = ['MCX:CRUDEOIL22FEBFUT', 'MCX:GOLD22APRFUT']
fyersSocket.subscribe(symbol=symbol, data_type=data_type)
print(f'subscribed to f{symbol}')
fyersSocket.background_flag = False

# {This is used in order to keep your Websocket Thread Open and also do the remaining functionality as expected or
# other method calls}
fyersSocket.keep_running()
