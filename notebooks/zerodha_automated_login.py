import urllib.parse as urlparse
from time import sleep

# zerodha
from kiteconnect import KiteConnect
from selenium import webdriver
# chrome options class is used to manipulate various properties of Chrome driver
from selenium.webdriver.chrome.options import Options
# find the above condition/conntent by the xpath, id etc.
from selenium.webdriver.common.by import By
# finds that content
from selenium.webdriver.support import expected_conditions as EC
# waits till the content loads
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

# credentials
api_key = 'yabfgbz64qlv3caj'
api_secret = 'uzf6ae99a9qikgrba7spwejb8g67fgcj'
account_username = 'DG2081'
account_password = '1Jun2019'
account_two_fa = int(986008)

kite = KiteConnect(api_key=api_key)

options = Options()
options.add_argument('--headless')
options.add_argument('--disable-gpu')

driver = webdriver.Chrome(ChromeDriverManager().install())
# driver = webdriver.Chrome(
#    executable_path='/Users/entirety/Desktop/chromedriver', options=options)

driver.get(kite.login_url())

# //tagname[@attribute='value']
# tagname = div,

# identify login section
form = WebDriverWait(driver, 10).until(
    EC.visibility_of_element_located((By.XPATH, '//div[@class="login-form"]')))

# enter the ID
driver.find_element_by_xpath("//input[@type='text']").send_keys(
    account_username)

# enter the password
driver.find_element_by_xpath("//input[@type='password']").send_keys(
    account_password)

# submit
driver.find_element_by_xpath("//button[@type='submit']").click()

# sleep for a second so that the page can submit and proceed to upcoming question (2fa)
sleep(1)
form = WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
    (By.XPATH, '//div[@class="login-form"]//form')))

# identify login section for 2fa
# enter the 2fa code
driver.find_element_by_xpath("//input[@type='password']").send_keys(
    account_two_fa)

# submit
driver.find_element_by_xpath("//button[@type='submit']").click()
sleep(1)
current_url = driver.current_url

driver.close()

parsed = urlparse.urlparse(current_url)
request_token = urlparse.parse_qs(parsed.query)['request_token'][0]

access_token = \
    kite.generate_session(request_token=request_token, api_secret=api_secret)[
        'access_token']

kite.set_access_token(access_token)

response = kite.ltp('MCX:CRUDEOIL22FEBFUT')

print(f'response ltp : {response}')
