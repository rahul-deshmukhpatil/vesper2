from datetime import datetime
from ratelimit import limits, sleep_and_retry


class MyClass:
    def __init__(self):
        pass

    @sleep_and_retry
    @limits(calls=3, period=1)
    def test_method(self):
        timestamp = datetime.now()
        print(f'{timestamp}')


t = MyClass()
while True:
    t.test_method()
