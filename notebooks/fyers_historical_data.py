from enum import IntEnum
from unittest.mock import Mock

from vesper.connectors.common.fyers.fyers_login import FyersLogin
from vesper.utils.loggers import logger


class HistoricalDownload(FyersLogin):
    def __init__(self):
        FyersLogin.__init__(self)
        self.cmd_line = Mock()
        self.cmd_line.app = 'test_hist_download'
        self.cmd_line.instance = 'fyers'
        FyersLogin.setup(self)

    def download(self, *args, **kwargs):
        try:
            return self.fyersModel.history(*args, **kwargs)
        except Exception as ex:
            logger.error(f'Exception occurred : {ex}')

    def close(self):
        FyersLogin.close(self)


hd = HistoricalDownload()

symbol = 'NSE:SBIN-EQ'
# symbol = 'NSE:SBIN22FEBFUT'
# symbol = 'NSE:SBIN22JANFUT'
range_from = '2016-11-16'
range_to = '2022-02-18'
resolutions = ['1', '3', '5', '15', '30', '60', 'D']


class FyersDateFmt(IntEnum):
    EPOCH = 0
    YYYY_MM_DD = 1


data = {}
for resolution in resolutions:
    # symbol = input('new instrument_token : ') or symbol

    args_data = {
        "symbol": symbol,
        "resolution": resolution,
        "date_format": FyersDateFmt.YYYY_MM_DD.value,
        "range_from": range_from,
        "range_to": range_to,
        "cont_flag": True,
    }

    instr_data = hd.download(args_data)
    logger.info(f'downloaded {symbol} : {resolution}')
    if instr_data['s'] == 'ok':
        data[resolution] = instr_data['candles']
    else:
        data[resolution] = instr_data

hd.close()
