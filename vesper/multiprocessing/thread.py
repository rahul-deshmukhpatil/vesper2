from threading import Thread

from vesper.utils.loggers import getLogger
logger = getLogger()


class VesperThread(Thread):

    def __init__(self):
        super(VesperThread, self).__init__()

        self.setup_done = False
        self.running = False
        pass

    def start(self):
        if not self.setup_done:
            logger.error(
                f'could not run instance of {self.__class__.__name__} as setup is not complete')
            return

        try:
            self.running = True
            super(VesperThread, self).start()
        except RuntimeError as runtime_error:
            self.running = False
            self.setup_done = False
            logger.error(
                f'exception occurred while trying to run instance of {self.__class__.__name__} : ', str(runtime_error))

    def stop(self):
        self.running = False

    def close(self):
        self.running = False
        super(VesperThread, self).join()
