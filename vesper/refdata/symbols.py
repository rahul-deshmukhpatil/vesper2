from .expiry import Expiry
from datetime import date
from vesper.types.types import Exchange
import itertools

nse_etf = ['ABSLBANETF', 'AXISGOLD', 'BANKBEES', 'BSLGOLDETF', 'BSLNIFTY', 'CPSEETF', 'EBANK', 'EBBETF0423', 'EBBETF0430', 'EQ30', 'GOLDBEES', 'GOLDSHARE', 'HDFCMFGETF', 'HDFCNIFETF', 'HDFCSENETF', 'HNGSNGBEES', 'ICICIGOLD', 'ICICILOVOL', 'ICICIMCAP', 'ICICINF100', 'ICICINIFTY', 'ICICINV20', 'ICICISENSX', 'IDBIGOLD', 'INFRABEES', 'IVZINGOLD', 'IVZINNIFTY', 'JETFREIGHT-SM', 'JUNIORBEES', 'KOTAKBKETF',
           'KOTAKGOLD', 'KOTAKNIFTY', 'KOTAKNV20', 'LICNETFGSC', 'LICNETFN50', 'LICNETFSEN', 'LIQUIDBEES', 'LIQUIDETF', 'M100', 'M50', 'MAN50ETF', 'N100', 'NETF', 'NETFCONSUM', 'NETFDIVOPP', 'NETFIT', 'NETFLTGILT', 'NETFMID150', 'NETFNIF100', 'NETFNV20', 'NIFTYBEES', 'NIFTYEES', 'PSUBNKBEES', 'QNIFTY', 'SBIETFQLTY', 'SETF10GILT', 'SETFGOLD', 'SETFNIF50', 'SETFNIFBK', 'SETFNN50', 'SHARIABEES', 'UTINIFTETF', 'UTISENSETF']

bse_etf = ['ABSLBANETF', 'BANKBEES', 'BSLGOLDETF', 'BSLSENETFG', 'CPSEETF', 'EBBETF0423', 'EBBETF0430', 'HDFCMFGETF', 'HDFCNIFETF', 'ICICINF100', 'ICICINIFTY', 'ICICINV20', 'ICICISENSX', 'JUNIORBEES', 'LICNETFN50',
           'LICNETFSEN', 'LIQUIDETF', 'MAN50ETF', 'MANXT50ETF', 'NETFNIF100', 'NETFSENSEX', 'NETFSNX150', 'NIFTYBEES', 'NIFTYEES', 'PSUBNKBEES', 'SBISENSEX', 'SETFBSE100', 'SETFSN50', 'SHARIABEES', 'SXETF', 'UTINIFTETF', 'UTISENSETF']

index = ['NIFTY 200', 'NIFTY ENERGY', 'NIFTY AUTO', 'NIFTY CONSUMPTION', 'NIFTY IT', 'NIFTY BANK', 'NIFTY50 PR 1X INV',
         'NIFTY50 PR 2X LEV', 'CPSE INDEX', 'NIFTY SERV SECTOR', 'NIFTY50 DIV POINT', 'NIFTY METAL', 'NIFTY MIDCAP 100',
         'HANGSENG BEES-NAV', 'INDIA VIX', 'NIFTY FMCG', 'NIFTY MNC', 'NIFTY JUNIOR', 'NIFTY50 TR 1X INV',
         'NIFTY MIDCAP 50', 'SENSEX', 'NIFTY COMMODITIES', 'NIFTY FIN SERVICE', 'LIX 15', 'NIFTY REALTY',
         'NIFTY50 TR 2X LEV', 'NIFTY SMALL 100', 'NIFTY INFRA', 'NV20', 'NIFTY MEDIA', 'NIFTY PSE', 'NIFTY PSU BANK',
         'NIFTY DIV OPPS 50', 'NIFTY 100', 'NIFTY 50', 'NI15', 'NIFTY PHARMA']

main_indices = ['NIFTY 50', 'NIFTY BANK', 'INDIA VIX']

nifty50 = ['BHARTIARTL', 'TITAN', 'TATASTEEL', 'BPCL', 'M&M', 'POWERGRID', 'RBLBANK', 'GAIL', 'HINDUNILVR', 'KOTAKBANK',
           'SBIN', 'ADANIPORTS', 'DRREDDY', 'BRITANNIA', 'TCS', 'ASIANPAINT', 'RELIANCE', 'CIPLA', 'AXISBANK', 'LT',
           'EICHERMOT', 'BAJAJ-AUTO', 'HCLTECH', 'IOC', 'IBULHSGFIN', 'WIPRO', 'UPL', 'GRASIM', 'FEDERALBNK', 'ONGC',
           'BANKBARODA', 'BAJAJFINSV', 'TECHM', 'IDFCFIRSTB', 'JSWSTEEL', 'SUNPHARMA', 'HEROMOTOCO', 'ZEEL', 'HDFCBANK',
           'TATAMOTORS', 'HINDALCO', 'ULTRACEMCO', 'YESBANK', 'ITC', 'MARUTI', 'BAJFINANCE', 'HDFC', 'ICICIBANK',
           'INDUSINDBK', 'INFRATEL', 'INFY', 'NTPC', 'VEDL', 'COALINDIA', 'PNB']

nifty200 = ['DIVISLAB', 'JINDALSTEL', 'ENDURANCE', 'PGHH', 'GRUH', 'UNIONBANK', 'RAMCOCEM', 'ICICIGI', 'L&TFH',
            'IBVENTURES', 'BERGEPAINT', 'HINDPETRO', 'VOLTAS', 'GODREJCP', 'GRAPHITE', 'NBCC', 'MCDOWELL-N', 'SYNGENE',
            'PETRONET', 'GMRINFRA', 'CANBK', 'BBTC', 'FCONSUMER', 'MFSL', 'MRF', 'FRETAIL', 'OIL', 'DBL', 'PFIZER',
            'BANKINDIA', 'CHOLAFIN', 'JSWENERGY', 'EXIDEIND', 'PAGEIND', 'RELCAPITAL', 'HDFCAMC', 'INDHOTEL', 'INDIGO',
            'BHARATFORG', 'MOTHERSUMI', 'COLPAL', 'LTTS', 'BAJAJHLDNG', 'JUBILANT', 'ABCAPITAL', 'CADILAHC',
            'PIDILITIND', 'APOLLOTYRE', 'AUBANK', 'NATCOPHARM', 'AMARAJABAT', 'QUESS', 'SBILIFE', 'SHREECEM', 'ESCORTS',
            'DLF', 'MPHASIS', 'LUPIN', 'TATAPOWER', 'GSPL', 'TATAGLOBAL', 'DISHTV', 'MINDTREE', 'NMDC', 'VARROC', 'IGL',
            'GLENMARK', 'MARICO', 'COROMANDEL', 'TORNTPHARM', 'ASHOKLEY', 'MRPL', 'OBEROIRLTY', 'BIOCON', 'SAIL', 'MGL',
            'ACC', 'CONCOR', 'IDEA', 'TATACHEM', 'PEL', 'MUTHOOTFIN', 'TATAMTRDVR', 'OFSS', 'EDELWEISS', 'HEXAWARE',
            'BEL', 'CROMPTON', 'M&MFIN', 'AJANTPHARM', 'SPARC', 'RELINFRA', 'RECLTD', 'GODREJAGRO', 'APOLLOHOSP',
            'ADANIPOWER', 'SRF', 'BHEL', 'IDBI', 'HEG', 'ENGINERSIN', 'LTI', 'PRESTIGE', 'NHPC', 'BATAINDIA',
            'AUROPHARMA', 'NAUKRI', 'HUDCO', 'TORNTPOWER', 'NIACL', 'GODREJIND', 'AMBUJACEM', 'SUNTV', 'HDFCLIFE',
            'MANAPPURAM', 'DHFL', 'BALKRISIND', 'ABFRL', 'PNBHOUSING', 'HINDZINC', 'STRTECH', 'RAJESHEXPO', 'DMART',
            'HAVELLS', 'LICHSGFIN', 'JUBLFOOD', 'DABUR', 'CUMMINSIND', 'NATIONALUM', 'ABB', 'GICRE', 'UBL', 'BOSCHLTD',
            'INDIANB', 'CASTROLIND', 'PIIND', 'PFC', 'ICICIPRULI', 'BANDHANBNK', 'ALKEM', 'SIEMENS', 'TVSMOTOR',
            'GSKCONS', 'SRTRANSFIN', 'RPOWER', 'VGUARD']

nifty500 = ['3MINDIA', 'AAVAS', 'ADANIGREEN', 'ADANITRANS', 'ADVENZYMES', 'AEGISCHEM', 'AIAENG', 'AKZOINDIA', 'ALBK',
            'ALLCARGO', 'ANDHRABANK', 'APLAPOLLO', 'APLLTD', 'ASHOKA', 'ASTERDM', 'ASTRAL', 'ASTRAZEN', 'ATUL',
            'AVANTIFEED', 'BAJAJCON', 'BAJAJELEC', 'BALMLAWRIE', 'BALRAMCHIN', 'BASF', 'BDL', 'BEML', 'BIRLACORPN',
            'BLISSGVS', 'BLUEDART', 'BLUESTARCO', 'BOMDYEING', 'BRIGADE', 'BSE', 'CANFINHOME', 'CAPLIPOINT',
            'CARBORUNIV', 'CARERATING', 'CCL', 'CDSL', 'CEATLTD', 'CENTRALBK', 'CENTURYPLY', 'CERA', 'CESC', 'CGPOWER',
            'CHAMBLFERT', 'CHENNPETRO', 'CHOLAHLDNG', 'COCHINSHIP', 'COFFEEDAY', 'CORPBANK', 'COX&KINGS', 'CREDITACC',
            'CRISIL', 'CUB', 'CYIENT', 'DBCORP', 'DCAL', 'DCBBANK', 'DCMSHRIRAM', 'DEEPAKFERT', 'DEEPAKNTR',
            'DELTACORP', 'DIXON', 'ECLERX', 'EIDPARRY', 'EIHOTEL', 'ELGIEQUIP', 'EMAMILTD', 'EQUITAS', 'ERIS',
            'ESSELPACK', 'FDC', 'FINCABLES', 'FINEORG', 'FINPIPE', 'FLFL', 'FORTIS', 'FSL', 'GALAXYSURF', 'GAYAPROJ',
            'GDL', 'GEPIL', 'GESHIP', 'GET&D', 'GHCL', 'GILLETTE', 'GLAXO', 'GMDCLTD', 'GNFC', 'GODFRYPHLP',
            'GODREJPROP', 'GPPL', 'GRANULES', 'GREAVESCOT', 'GRINDWELL', 'GSFC', 'GUJALKALI', 'GUJFLUORO', 'GUJGASLTD',
            'GULFOILLUB', 'HAL', 'HATHWAY', 'HATSUN', 'HEIDELBERG', 'HERITGFOOD', 'HFCL', 'HIMATSEIDE', 'HINDCOPPER',
            'HONAUT', 'HSCL', 'IBREALEST', 'IBULISL', 'ICRA', 'IDFC', 'IEX', 'IFBIND', 'IFCI', 'INDIACEM', 'INDOCO',
            'INDOSTAR', 'INFIBEAM', 'INOXLEISUR', 'INOXWIND', 'INTELLECT', 'IOB', 'IPCALAB', 'IRB', 'IRCON', 'ISEC',
            'ITDC', 'ITDCEM', 'ITI', 'JAGRAN', 'JAICORPLTD', 'JAMNAAUTO', 'JBCHEPHARM', 'JETAIRWAYS', 'JINDALSAW',
            'JISLJALEQS', 'J&KBANK', 'JKCEMENT', 'JKLAKSHMI', 'JKPAPER', 'JKTYRE', 'JMFINANCIL', 'JPASSOCIAT', 'JSL',
            'JSLHISAR', 'JUSTDIAL', 'JYOTHYLAB', 'KAJARIACER', 'KALPATPOWR', 'KANSAINER', 'KARURVYSYA', 'KEC', 'KEI',
            'KIOCL', 'KIRLOSENG', 'KNRCON', 'KOLTEPATIL', 'KPRMILL', 'KRBL', 'KSCL', 'KTKBANK', 'LAKSHVILAS',
            'LALPATHLAB', 'LAURUSLABS', 'LAXMIMACH', 'LEMONTREE', 'LINDEINDIA', 'LUXIND', 'MAGMA', 'MAHABANK',
            'MAHINDCIE', 'MAHLOG', 'MAHSCOOTER', 'MAHSEAMLES', 'MASFIN', 'MAXINDIA', 'MERCK', 'MHRIL', 'MINDACORP',
            'MINDAIND', 'MMTC', 'MOIL', 'MONSANTO', 'MOTILALOFS', 'NAVINFLUOR', 'NBVENTURES', 'NCC', 'NESCO',
            'NETWORK18', 'NFL', 'NH', 'NIITTECH', 'NILKAMAL', 'NLCINDIA', 'OMAXE', 'ORIENTBANK', 'ORIENTCEM',
            'ORIENTELEC', 'PARAGMILK', 'PCJEWELLER', 'PERSISTENT', 'PHILIPCARB', 'PHOENIXLTD', 'PNCINFRA', 'PRAJIND',
            'PRSMJOHNSN', 'PTC', 'PVR', 'RADICO', 'RAIN', 'RALLIS', 'RAYMOND', 'RCF', 'RCOM', 'REDINGTON', 'RELAXO',
            'RENUKA', 'REPCOHOME', 'RHFL', 'RITES', 'RKFORGE', 'RNAM', 'RUPA', 'SADBHAV', 'SANOFI', 'SCHAEFFLER', 'SCI',
            'SFL', 'SHANKARA', 'SHARDACROP', 'SHILPAMED', 'SHK', 'SHOPERSTOP', 'SHRIRAMCIT', 'SIS', 'SJVN', 'SKFINDIA',
            'SOBHA', 'SOLARINDS', 'SONATSOFTW', 'SOUTHBANK', 'SPTL', 'SREINFRA', 'STAR', 'STARCEMENT', 'SUDARSCHEM',
            'SUNCLAYLTD', 'SUNDARMFIN', 'SUNDRMFAST', 'SUNTECK', 'SUPRAJIT', 'SUPREMEIND', 'SUVEN', 'SUZLON',
            'SWANENERGY', 'SYMPHONY', 'SYNDIBANK', 'TAKE', 'TATACOFFEE', 'TATAELXSI', 'TATAINVEST', 'TCNSBRANDS',
            'TEAMLEASE', 'THERMAX', 'THOMASCOOK', 'THYROCARE', 'TIINDIA', 'TIMETECHNO', 'TIMKEN', 'TNPL', 'TRENT',
            'TRIDENT', 'TRITURBINE', 'TTKPRESTIG', 'TV18BRDCST', 'TVTODAY', 'UCOBANK', 'UFLEX', 'UJJIVAN', 'VAKRANGEE',
            'VBL', 'VENKEYS', 'VINATIORGA', 'VIPIND', 'VMART', 'VRLLOG', 'VSTIND', 'VTL', 'WABAG', 'WABCOINDIA',
            'WELCORP', 'WELSPUNIND', 'WHIRLPOOL', 'WOCKPHARMA', 'ZENSARTECH', 'ZYDUSWELL']


class Symbols:
    instruments = {}
    symbols = {}
    all_symbols = {}
    futures = {}
    cds = {}
    options = {}

    @staticmethod
    def get_dict(dictionary, *args):
        args_list = list(args)
        key = args_list.pop(0)
        if key not in dictionary:
            dictionary[key] = {}

        if len(args_list):
            return Symbols.get_dict(dictionary[key], *tuple(args_list))

        return dictionary[key]

    @staticmethod
    def insert_refdata(dictionary, symbol, refdata):
        if symbol in dictionary:
            refdata_present = dictionary[symbol]

            if refdata_present.exchange_id == refdata.exchange_id \
                    and refdata_present.instrument_id == refdata.instrument_id:
                # already present key
                return
            else:
                assert False, f'symbol: {symbol} already present in the dictionary !!!'
        dictionary[symbol] = refdata

    @staticmethod
    def categories(refdata):

        # USDINR20MAY20JUNFUT shares same symbol with USDINR20MAYFUT
        # or any(refdata.symbol.count(month) >= 2 for month in
        #       ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']):
        if refdata.exchange_id == 0 \
                or refdata.exchange == Exchange.MCX \
                or any(keyword in refdata.symbol for keyword in [' STD ', ' CNV ']):
            return

        Symbols.instruments[refdata.instrument_id] = refdata

        all_symbols_dict = Symbols.get_dict(
            Symbols.all_symbols, refdata.exchange)
        Symbols.insert_refdata(all_symbols_dict, refdata.symbol, refdata)

        if refdata.instr_type == 'EQ':
            exchange_dict = Symbols.get_dict(
                Symbols.symbols, refdata.exchange, refdata.instr_type)
            Symbols.insert_refdata(exchange_dict, refdata.symbol, refdata)
        elif refdata.instr_type == 'FUT':
            exchange_dict = Symbols.get_dict(
                Symbols.symbols, refdata.exchange, refdata.instr_type, refdata.underlying)
            Symbols.insert_refdata(exchange_dict, refdata.expiry, refdata)
        elif refdata.instr_type in ['CE', 'PE'] and refdata.exchange != 'BCD':
            exchange_dict = Symbols.get_dict(Symbols.symbols, refdata.exchange, refdata.instr_type, refdata.underlying,
                                             refdata.expiry)
            Symbols.insert_refdata(exchange_dict, refdata.strike, refdata)

    @staticmethod
    def get_future_symbols(exchange, underlyings, from_date=None, count=1):
        symbols = []

        if from_date is None:
            from_date = date.today()

        expiries = Expiry.get_expiries(exchange, from_date, count)
        exchange_dict = Symbols.symbols[exchange]['FUT']

        for underlying, expiry in list(itertools.product(underlyings, expiries)):
            try:
                futures_refdata = exchange_dict[underlying][expiry]
                symbols.append(futures_refdata.symbol)
            except:
                pass

        return symbols

    @staticmethod
    def get_options_with_type(exchange, underlyings, option_type, from_strike, till_strike, from_date, count):
        symbols = []
        exchange_dict = Symbols.symbols[exchange][option_type]

        expiries = Expiry.get_expiries(exchange, from_date, count)
        for underlying, expiry in list(itertools.product(underlyings, expiries)):
            all_options = exchange_dict[underlying][expiry]
            for strike, options_refdata in all_options.items():
                if from_strike <= strike <= till_strike:
                    symbols.append(options_refdata.symbol)

        return symbols

    @staticmethod
    def get_options_symbols(exchange, underlyings, from_strike, till_strike, from_date=None, count=1):
        if from_date is None:
            from_date = date.today()

        symbols = Symbols.get_options_with_type(
            exchange, underlyings, 'CE', from_strike, till_strike, from_date, count)
        symbols = symbols + Symbols.get_options_with_type(exchange, underlyings, 'PE', from_strike, till_strike,
                                                          from_date, count)
        return symbols
