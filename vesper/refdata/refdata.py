import json
from datetime import date

from vesper.refdata.symbols import Symbols
from vesper.types.types import Exchange


class Refdata:

    @staticmethod
    def load(instruments):
        for instrument in instruments:
            refdata = Refdata(instrument)
            Symbols.categories(refdata)

    @staticmethod
    def load_ref_data(refdata_file):
        with open(refdata_file, 'r') as refdata:
            json_instruments = []
            instruments = refdata.readlines()
            for instrument in instruments:
                json_instruments.append(json.loads(instrument))
            Refdata.load(json_instruments)

    def __init__(self, instrument):
        self.exchange = Exchange[str(instrument['exchange'])]
        self.symbol = str(instrument['tradingsymbol'])

        self.segment = str(instrument['segment'])
        self.instr_type = str(instrument['instrument_type'])
        self.underlying = str(instrument['name'])

        self.exchange_id = int(instrument['exchange_token'])
        self.instrument_id = int(instrument['instrument_token'])

        self.lot = None
        self.tick = float(instrument['tick_size']), 4
        self.strike = round(float(instrument['strike']), 4)

        expiry = str(instrument['expiry'])
        if expiry:
            self.expiry = date.fromisoformat(expiry)

        self.ffm_shares = 0

