from vesper.trad_engine.constants import TradingEngine
from vesper.trad_engine.supertrend import Supertrend


def get_trading_engine_class(engine):
    engines = {
        TradingEngine.SUPERTREND: Supertrend,
    }
    return engines[engine.upper()]
