import pandas as pd

from vesper.date.date import get_epoch, get_int_epoch
from vesper.utils.loggers import logger


class Order:
    def __init__(self, symbol, side, quantity, trigger_price, tag):
        self.symbol = symbol
        self.side = side
        self.quantity = quantity
        self.trigger_price = trigger_price
        self.tag = tag


class Supertrend:
    def __init__(self):
        self.last_epoch = 0
        self.params = None
        self.params_file = 'supertrend.csv'
        self.instances = {}

    def setup(self):
        self.update_params()
        pass

    def update_params(self):
        self.params = pd.read_csv(
            self.params_file, delimiter=',', header=0, encoding='utf-8')
        for index, param in self.params.iterrows():
            self.instances[param.symbol] = param

    def update_order_status(self):
        pass

    def orders_to_send(self):
        orders = []

        current_epoch = get_int_epoch()
        if self.last_epoch == current_epoch:
            return orders

        self.last_epoch = current_epoch

        for symbol, params in self.instances.items():
            if 0 == current_epoch % params.frequency and params.active:
                orders.append(Order(symbol, params.side, params.quantity, 0, symbol+str(current_epoch)))
                logger.info(f'send order for {symbol}')
        return orders

    def supertrend_loop(self):
        self.update_params()
        self.update_order_status()
        return self.orders_to_send()

    def run(self):
        while True:
            self.supertrend_loop()

    def close(self):
        pass
