from vesper.mktdata.feed import Feed
from vesper.trad_engine.strategies import Strategies
from vesper.utils.argsparser import Cmd
from vesper.apps.app import App, setup_and_run
from vesper.utils.exception import VesperException
from vesper.utils.vesper_signals import register_signals

from vesper.utils.loggers import getLogger

logger = getLogger()


class TradEngineSignal(VesperException):
    def __init__(self, signal_num):
        super(TradEngineSignal, self).__init__(
            f'received signal : {signal_num}')


def end_process(signal_num, tb_frames):
    error = f'signal received {signal_num}'
    raise TradEngineSignal(error).with_traceback(None)


def add_common_fields():
    Cmd.add_argument('--strategy', required=False,
                     type=str, help="strategy name")


class TradingEngine(App):
    add_common_fields()

    def __init__(self):
        super(TradingEngine, self).__init__('strategies')
        self.feed = Feed(self)
        self.strategies = Strategies(self)

    def setup(self):
        register_signals(end_process)
        self.feed.setup()
        self.strategies.setup()

    def on_data_updated(self, ticks):
        self.strategies.on_data_updated()

    def subscribe(self, subscriptions):
        self.feed.subscribe(subscriptions)

    def run(self):
        try:
            self.feed.run()
        except TradEngineSignal as signal_received:
            self.close()

    def close(self):
        self.strategies.close()
        self.feed.close()
        pass


def start():
    trade_engine = TradingEngine()
    setup_and_run(trade_engine)


if __name__ == '__main__':
    start()
