from vesper.utils.loggers import getLogger

logger = getLogger()


class Strategies:
    @staticmethod
    def get_strategy(name):
        return globals()[name]

    def __init__(self, trad_engine):
        self.trad_engine = trad_engine
        self.instances = None

    def setup(self):
        self.instances = []
        self.add_def_strategy()
        for instance in self.instances:
            instance.setup()

    def add_def_strategy(self):
        self.add_strategy('EquityTa')

    def add_strategy(self, name):
        strategy_class = Strategies.get_strategy(name)
        strategy = strategy_class(self)
        self.instances.append(strategy)
        strategy.setup()

    def on_data_updated(self):
        for instance in self.instances:
            instance.on_data_updated()

    def close(self):
        for instance in self.instances:
            instance.close()

    def subscribe(self, subscriptions):
        self.trad_engine.subscribe(subscriptions)
