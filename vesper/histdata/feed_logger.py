import gzip
import json
from collections import namedtuple

from vesper.histdata.utils import get_hist_dir
from vesper.utils import jsonutils
from vesper.utils.argsparser import parser
from vesper.utils.constants import Broker
from vesper.utils.kafka_prod_cons import KafkaMessanger
from vesper.utils.loggers import logger

parser.add_argument('-d', '--histdir', required=True,
                    type=str, help="historical dir")


class FeedLogger(KafkaMessanger):
    @staticmethod
    def get_feed_file(feed_file_dir):
        return f'{feed_file_dir}/feed.gz'

    def __init__(self):
        self.cmd_line = parser.parse_known_args()[0]
        Topic = namedtuple('Topic', 'name file gzip_writer')

        def create_topic(topic):
            feed_file_dir = get_hist_dir(self.cmd_line, 'mktdata', topic)
            feed_file = FeedLogger.get_feed_file(feed_file_dir)
            gzip_writer = gzip.open(feed_file, 'at')
            return Topic(topic, feed_file, gzip_writer)

        self.topics = {x.name: create_topic(x.name) for x in Broker}
        KafkaMessanger.__init__(self, cons_topics=list(self.topics.keys()))

    def setup(self):
        KafkaMessanger.setup(self)

    def run(self):
        while True:
            messages = self.get_kafka_messages()
            for message in messages:
                logger.info(f'received data on topic : {message.topic}')
                json_str = json.dumps(
                    message.value, default=jsonutils.custom_datetime_encoder) \
                    + '\n'
                self.topics[message.topic].gzip_writer.write(json_str)

    def close(self):
        logger.info('closing the feed files')
        for topic in self.topics.values():
            topic.gzip_writer.flush()
            topic.gzip_writer.close()
        KafkaMessanger.close(self)
