import os.path
from datetime import datetime

from vesper.date.date import datetime2str
from vesper.utils.file_utils import create_dir


def get_apps_hist_dir(cmd_line):
    date = datetime2str(fmt='%Y%m%d')
    histdir = f'{cmd_line.histdir}/{date}/{cmd_line.app.lower()}/' \
              f'{cmd_line.instance.lower()}'
    create_dir(histdir)
    return histdir


def get_hist_dir_for_date(date, cmd_line, *args):
    date = datetime2str(timestamp=date, fmt='%Y%m%d')
    histdir = os.path.join(cmd_line.histdir, date)
    for arg in args:
        histdir = os.path.join(histdir, arg.lower())
    create_dir(histdir)
    return histdir


def get_hist_dir(cmd_line, app, instance, *args):
    return get_hist_dir_for_date(datetime.now(), cmd_line, app.lower(),
                                 instance.lower(),
                                 *args)
