from .app import App, setup_and_run
from vesper.utils.loggers import RefDataLogger
from vesper.zerodha.connect import Zerodha
from vesper.refdata.refdata import Refdata


class RefdataApp(App):
    def __init__(self):
        super(RefdataApp, self).__init__('refdata')
        self.zerodha = Zerodha()

    def setup(self):
        self.zerodha.login_zerodha()

    def run(self):
        instruments = self.zerodha.kiteconnect.instruments()
        Refdata.load(instruments)
        RefDataLogger.write(instruments)

        min_ref_data_lines = 10000
        assert(len(instruments) > min_ref_data_lines)

    def close(self):
        self.zerodha.close()


def start():
    refdata_app = RefdataApp()
    setup_and_run(refdata_app)
    refdata_app.close()


if __name__ == '__main__':
    start()
