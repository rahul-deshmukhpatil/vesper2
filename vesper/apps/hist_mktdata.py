from vesper.apps.app import App, setup_and_run
from vesper.connectors.hist_mktdata_manager import \
    get_broker_hist_mktdata_api_class
from vesper.utils.argsparser import Cmd
from vesper.utils.constants import AppType


class HistMktdata(App):
    def __init__(self):
        App.__init__(self, AppType.HIST_MKTDATA)
        BrokerApi = get_broker_hist_mktdata_api_class(Cmd.parse().instance)
        self.broker_api = BrokerApi()

    def setup(self):
        self.broker_api.setup()

    def run(self):
        self.broker_api.run()

    def close(self):
        self.broker_api.close()
        App.close(self)


def start():
    orderex = HistMktdata()
    setup_and_run(orderex)


if __name__ == '__main__':
    start()
