from vesper.apps.app import App, setup_and_run
from vesper.utils.constants import AppType


class TestApp(App):
    def __init__(self):
        App.__init__(self, AppType.TEST_APP)

    def setup(self):
        pass

    def run(self):
        try:
            pass
        except:
            self.close()
            return
        self.close()

    def close(self):
        App.close(self)


def start():
    orderex = TestApp()
    setup_and_run(orderex)


if __name__ == '__main__':
    start()
