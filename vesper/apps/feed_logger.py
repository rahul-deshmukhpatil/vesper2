from vesper.apps.app import App, setup_and_run
from vesper.histdata.feed_logger import FeedLogger
from vesper.utils.constants import AppType


class FeedLoggerApp(App):
    def __init__(self):
        App.__init__(self, AppType.FEED_LOGGER)
        self.feed_logger = FeedLogger()

    def setup(self):
        self.feed_logger.setup()

    def run(self):
        self.feed_logger.run()

    def close(self):
        self.feed_logger.close()
        App.close(self)


def start():
    feed_logger = FeedLoggerApp()
    setup_and_run(feed_logger)


if __name__ == '__main__':
    start()
