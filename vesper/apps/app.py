import os
import sys

from vesper.utils.argsparser import parser
from vesper.utils.exception import VesperException
from vesper.utils.loggers import Logger, logger
from vesper.utils.vesper_signals import register_signals

os.environ["TZ"] = "UTC"


class MktdataSignal(VesperException):
    def __init__(self, signal_num):
        super(MktdataSignal, self).__init__(f'received signal : {signal_num}')


def end_process(signal_num, tb_frames):
    error = f'signal received {signal_num}'
    raise MktdataSignal(error).with_traceback(None)


class App:
    def __init__(self, app_type):
        self.cmd_line = parser.parse_known_args()[0]
        self.app_type = app_type
        register_signals(end_process)

    def run(self):
        assert False and ", derived class should override this method"

    def start(self):
        try:
            result = self.run()
        except MktdataSignal as signal_received:
            logger.error(f'Received signal to stop')
            result = 0
        except VesperException as ex:
            logger.error(f'stopping vesper on exception : {str(ex)} \n'
                         f' traceback: {sys.exc_info()[2]}')
            result = -1

        logger.info('stopping App')
        self.close()
        return result

    def close(self):
        logger.info('closed App')
        Logger.close_all_loggers()


def setup_and_run(app_instance):
    app_instance.setup()
    return app_instance.start()
