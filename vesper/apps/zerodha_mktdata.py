from collections import defaultdict

from vesper.connectors.common.symbols import NIFTY_500, NewSymbols
from .app import App, setup_and_run
from vesper.utils.loggers import getLogger, RefDataLogger
from vesper.refdata.refdata import Refdata
from vesper.refdata.symbols import main_indices, nifty50, nifty200, nifty500, \
    Symbols, bse_etf, nse_etf
from vesper.utils.exception import VesperException
from vesper.utils.vesper_signals import register_signals
from vesper.zerodha.connect import Zerodha
from vesper.types.types import Exchange

logger = getLogger()


class ZerodhaMktdataSignal(VesperException):
    def __init__(self, signal_num):
        super(ZerodhaMktdataSignal, self).__init__(
            f'received signal : {signal_num}')


def end_process(signal_num, tb_frames):
    error = f'signal received {signal_num}'
    raise ZerodhaMktdataSignal(error).with_traceback(None)


class ZerodhaMktdata(App):
    mktdata_queue = None

    @staticmethod
    def on_json_ticks(ws, json_ticks):
        ZerodhaMktdata.mktdata_queue.put(json_ticks)

    def __init__(self, feed_logger):
        super(ZerodhaMktdata, self).__init__('zerodha_mktdata')
        self.symbols = None
        self.zerodha = Zerodha()
        ZerodhaMktdata.mktdata_queue = feed_logger.mktdata_queue

    def load_ref_data(self):
        instruments = self.zerodha.kiteconnect.instruments()
        RefDataLogger.write(instruments)
        Refdata.load(instruments)

    def register_callbacks(self):
        self.zerodha.kiteticker.on_tick = ZerodhaMktdata.on_json_ticks
        self.zerodha.kiteticker.on_ticks = ZerodhaMktdata.on_json_ticks
        self.zerodha.kiteticker.on_connect = Zerodha.on_connect
        self.zerodha.kiteticker.on_reconnect = Zerodha.on_reconnect

    @staticmethod
    def get_stock_indices_etfs():
        symbols = defaultdict(list)
        symbols[Exchange.BSE] += bse_etf
        symbols[Exchange.NSE] += nse_etf
        symbols[Exchange.NSE] += NewSymbols().get_nifty_symbols(
            NIFTY_500).Symbol
        symbols[Exchange.NSE] += (Exchange.NSE, main_indices)
        return symbols

    def subscribe_stocks_indices_etfs(self):
        for exchange, symbols in self.get_stock_indices_etfs().items():
            self.zerodha.subscribe(exchange, symbols)

    def subscribe_cd_fno(self):
        cds_futures = Symbols.get_future_symbols(
            Exchange.CDS, ['USDINR', 'EURINR', 'GBPINR', 'JPYINR'], count=3)
        self.zerodha.subscribe(Exchange.CDS, cds_futures)

        bcd_futures = Symbols.get_future_symbols(
            Exchange.BCD, ['USDINR', 'EURINR', 'GBPINR', 'JPYINR'], count=3)
        self.zerodha.subscribe(Exchange.BCD, bcd_futures)

        USDINR_SPOT = 75.0
        SPREAD = 2
        USDINR_LOW = round(USDINR_SPOT - SPREAD)
        USDINR_HIGH = USDINR_SPOT + SPREAD
        cds_options = Symbols.get_options_symbols(
            Exchange.CDS, ['USDINR'], USDINR_LOW, USDINR_HIGH)
        self.zerodha.subscribe(Exchange.CDS, cds_options)

        bcd_options = Symbols.get_options_symbols(
            Exchange.BCD, ['USDINR'], USDINR_LOW, USDINR_HIGH)
        self.zerodha.subscribe(Exchange.BCD, bcd_options)

    def subscribe_index_stock_fno(self):
        futures = Symbols.get_future_symbols(Exchange.NFO, main_indices +
                                             nifty200)
        self.zerodha.subscribe(Exchange.NFO, futures)

    def subscribe(self):
        self.zerodha.subscribe(Exchange.BSE, bse_etf)
        self.zerodha.subscribe(Exchange.NSE, nse_etf)
        self.zerodha.subscribe(Exchange.NSE, nifty50)
        self.zerodha.subscribe(Exchange.NSE, nifty200)
        self.zerodha.subscribe(Exchange.NSE, nifty500)

        futures = Symbols.get_future_symbols(Exchange.NFO, nifty50)
        futures = futures + Symbols.get_future_symbols(Exchange.NFO, nifty200)
        self.zerodha.subscribe(Exchange.NFO, futures)

        cds_futures = Symbols.get_future_symbols(
            Exchange.CDS, ['USDINR', 'EURINR', 'GBPINR', 'JPYINR'], count=3)
        self.zerodha.subscribe(Exchange.CDS, cds_futures)

        bcd_futures = Symbols.get_future_symbols(
            Exchange.BCD, ['USDINR', 'EURINR', 'GBPINR', 'JPYINR'], count=3)
        self.zerodha.subscribe(Exchange.BCD, bcd_futures)

        cds_options = Symbols.get_options_symbols(
            Exchange.CDS, ['USDINR'], round(72.0), 80)
        self.zerodha.subscribe(Exchange.CDS, cds_options)

        bcd_options = Symbols.get_options_symbols(
            Exchange.BCD, ['USDINR'], round(72.0), 80)
        self.zerodha.subscribe(Exchange.BCD, bcd_options)

    def setup(self):
        register_signals(end_process)
        self.zerodha.login_zerodha()
        self.register_callbacks()
        self.load_ref_data()
        self.subscribe()
        logger.info(f'setup done for {self.__class__.__name__}')

    def run(self):
        # runs into infinite loop
        # first callback on_connect subscribes instruments
        logger.info(f'running {self.__class__.__name__}')
        try:
            self.zerodha.kiteticker.connect(threaded=True)
        except ZerodhaMktdataSignal as singal_received:
            self.close()

    def close(self):
        self.zerodha.close()
        ZerodhaMktdata.mktdata_queue = None
        logger.info(f'closed {self.__class__.__name__}')


def start():
    setup_and_run(ZerodhaMktdata())


if __name__ == '__main__':
    start()
