from vesper.apps.app import App, setup_and_run
from vesper.trad_engine.constants import TradingEngine
from vesper.trad_engine.trading_engine_manager import get_trading_engine_class
from vesper.utils.constants import AppType


class TradingEngineApp(App):
    def __init__(self):
        App.__init__(self, AppType.TRADING_ENGINE)
        trading_engine_class = get_trading_engine_class(
            TradingEngine.SUPERTREND)
        self.trading_engine = trading_engine_class()

    def setup(self):
        self.trading_engine.setup()
        pass

    def run(self):
        try:
            self.trading_engine.run()
            pass
        except:
            self.close()
            return
        self.close()

    def close(self):
        self.trading_engine.close()
        App.close(self)


def start():
    trading_engine = TradingEngineApp()
    setup_and_run(trading_engine)


if __name__ == '__main__':
    start()
