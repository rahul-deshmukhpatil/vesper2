from vesper.apps.app import App, setup_and_run
from vesper.connectors.mktdata_manager import get_broker_mktdata_api_class
from vesper.utils.argsparser import Cmd
from vesper.utils.constants import AppType


class Mktdata(App):
    def __init__(self):
        App.__init__(self, AppType.MKTDATA)
        BrokerApi = get_broker_mktdata_api_class(Cmd.parse().instance)
        self.broker_api = BrokerApi()

    def setup(self):
        self.broker_api.setup()
        self.broker_api.load_refdata()
        self.broker_api.subscribe()

    def run(self):
        self.broker_api.run()

    def close(self):
        self.broker_api.close()
        App.close(self)


def start():
    orderex = Mktdata()
    setup_and_run(orderex)


if __name__ == '__main__':
    start()
