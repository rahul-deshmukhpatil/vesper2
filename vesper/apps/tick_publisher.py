import pytz
from arctic import TICK_STORE, Arctic

import pandas as pd

from vesper.date.date import str2datetime
from vesper.mktdata.feed import CoreFeed
from vesper.refdata.symbols import Symbols
from vesper.strategy.books import get_book
from vesper.types.types import TickMessage
from vesper.utils.argsparser import Cmd
from vesper.apps.app import App, setup_and_run
from vesper.utils.exception import VesperException
from vesper.utils.vesper_signals import register_signals

from vesper.utils.loggers import getLogger

logger = getLogger()


class TickPublisherSignal(VesperException):
    def __init__(self, signal_num):
        super(TickPublisherSignal, self).__init__(
            f'received signal : {signal_num}')


def end_process(signal_num, tb_frames):
    error = f'signal received {signal_num}'
    raise TickPublisherSignal(error).with_traceback(None)


def add_common_fields():
    Cmd.add_argument('--strategy', required=False,
                     type=str, help="strategy name")


class TickPublisher(App):
    add_common_fields()

    def __init__(self):
        super(TickPublisher, self).__init__('strategies')
        self.feed = CoreFeed(self)
        self.kafka_publisher = None
        self.arctic = Arctic('localhost')
        self.nse_store = None
        self.subscriptions = {}
        self.timezone = None

    def setup_arctic(self):
        self.arctic.initialize_library('NSE', lib_type=TICK_STORE)
        self.nse_store = self.arctic['NSE']

    def setup(self):
        register_signals(end_process)
        self.setup_arctic()
        self.feed.setup()
        self.timezone = pytz.timezone("Europe/Amsterdam")
        # self.kafka_publisher = KafkaProducer(bootstrap_servers=['localhost:9092'])

    def create_sub(self, instrument_id):
        refdata = Symbols.instruments[instrument_id]
        self.subscriptions[instrument_id] = get_book(
            refdata.exchange, refdata.symbol)
        return self.subscriptions[instrument_id]

    def get_subscription(self, instrument_id):
        try:
            return self.subscriptions[instrument_id]
        except KeyError:
            return self.create_sub(instrument_id)

    @staticmethod
    def get_levels(suffix, parsed_tick, levels):
        for i, level in enumerate(levels):
            for k, v in level.items():
                parsed_tick[f'{suffix}{k}{i}'] = v

    @staticmethod
    def parsed_raw_tick(raw_tick):
        parsed_tick = {}
        for key, value in raw_tick.items():
            if type(value) in [int, float]:
                parsed_tick[key] = value
            elif type(value) is dict and key == 'depth':
                TickPublisher.get_levels(
                    'b', parsed_tick, raw_tick['depth']['buy'])
                TickPublisher.get_levels(
                    's', parsed_tick, raw_tick['depth']['sell'])
            elif type(value) is dict and key == 'ohlc':
                parsed_tick.update(raw_tick[key])
            elif key == 'tradable':
                parsed_tick[key] = int(value)
            else:
                # print(f'skipping {key} : {value}')
                pass

        parsed_tick['type'] = TickMessage.DEPTH.value
        return [parsed_tick]

    @staticmethod
    def parsed_pv_tick(pv_data):
        pv_dict = pv_data.__dict__
        return [pv_dict]

    def create_parsed_ticks(self, instrument_data, raw_tick):
        local_timestamp = str2datetime(
            raw_tick['timestamp'], fmt='%Y-%m-%d %H:%M:%S')
        local_timestamp = self.timezone.localize(local_timestamp)
        tick_date_index = [local_timestamp]

        parsed_tick = TickPublisher.parsed_raw_tick(raw_tick)
        data = pd.DataFrame(parsed_tick, index=tick_date_index)

        parsed_pv_tick = TickPublisher.parsed_pv_tick(
            instrument_data.pv.pv_data)
        pv_data = pd.DataFrame(parsed_pv_tick, index=tick_date_index)
        return data, pv_data

    def on_ticks(self, ticks):
        for tick in ticks:
            # write into tick store
            instrument_id = tick['instrument_token']
            instrument_data = self.get_subscription(instrument_id)
            instrument_data.update_zerodha_tick(tick)

            data, pv_data = self.create_parsed_ticks(instrument_data, tick)
            self.nse_store.write(instrument_data.exchange_symbol, data)
            self.nse_store.write(instrument_data.exchange_symbol, pv_data)

    def on_data_updated(self, ticks):
        # json_str = json.dumps(ticks, default=jsonutils.custom_datetime_encoder) + '\n'
        # self.kafka_publisher.send(self.topic, value=json_str.encode('utf-8'))
        pass

    def subscribe(self, subscriptions):
        self.feed.subscribe(subscriptions)

    def run(self):
        try:
            self.feed.run()
        except TickPublisherSignal as signal_received:
            self.close()

    def close(self):
        self.feed.close()
        pass


def start():
    trade_engine = TickPublisher()
    setup_and_run(trade_engine)


if __name__ == '__main__':
    start()
