from vesper.apps.app import App, setup_and_run
from vesper.connectors.ordering_manager import get_broker_order_api_class
from vesper.utils.constants import Broker, AppType


class OrderEx(App):
    def __init__(self):
        App.__init__(self, AppType.ORDER_EX)
        BrokerApi = get_broker_order_api_class(Broker.KOTAK)
        self.broker_api = BrokerApi()

    def setup(self):
        self.broker_api.setup()
        pass

    def run(self):
        self.broker_api.run()

    def close(self):
        self.broker_api.close()
        App.close(self)


def start():
    orderex = OrderEx()
    setup_and_run(orderex)


if __name__ == '__main__':
    start()
