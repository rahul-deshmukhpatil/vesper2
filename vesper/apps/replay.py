import json

from kafka import KafkaProducer

from vesper.apps.app import App, setup_and_run
from vesper.backtester.backtester import Backtester
from vesper.utils import jsonutils
from vesper.utils.argsparser import Cmd
from vesper.utils.exception import VesperException
from vesper.utils.loggers import getLogger
from vesper.utils.vesper_signals import register_signals

logger = getLogger()


class ReplaySignal(VesperException):
    def __init__(self, signal_num):
        super(ReplaySignal, self).__init__(f'received signal : {signal_num}')


def end_process(signal_num, tb_frames):
    error = f'signal received {signal_num}'
    raise ReplaySignal(error).with_traceback(None)


def add_common_fields():
    Cmd.add_argument('--topic', required=False, type=str,
                     default='test-raw-feed', help="topic to publish on")


class Replay(App):
    cmd = add_common_fields()

    def __init__(self):
        super(Replay, self).__init__('replay')
        self.kafka_publisher = None
        self.topic = Cmd.parse().topic
        self.bt = Backtester(self)

    def setup(self):
        register_signals(end_process)
        self.bt.setup()
        self.kafka_publisher = KafkaProducer(
            bootstrap_servers=['localhost:9092'])

    def run(self):
        try:
            self.bt.run()
        except ReplaySignal as signal_received:
            self.close()

    def on_ticks(self, ticks):
        json_str = json.dumps(
            ticks, default=jsonutils.custom_datetime_encoder) + '\n'
        self.kafka_publisher.send(self.topic, value=json_str.encode('utf-8'))
        pass

    def close(self):
        pass


def start():
    replay = Replay()
    setup_and_run(replay)


if __name__ == '__main__':
    start()
