from vesper.apps.zerodha_mktdata import ZerodhaMktdata
from vesper.apps.app import App, setup_and_run
from vesper.mktdata.feed_logger import FeedLogger
from vesper.utils.exception import VesperException
from vesper.utils.vesper_signals import register_signals

from vesper.utils.loggers import getLogger

logger = getLogger()


class MktdataSignal(VesperException):
    def __init__(self, signal_num):
        super(MktdataSignal, self).__init__(f'received signal : {signal_num}')


def end_process(signal_num, tb_frames):
    error = f'signal received {signal_num}'
    raise MktdataSignal(error).with_traceback(None)


class Mktdata(App):
    def __init__(self):
        self.feed_logger = FeedLogger()
        self.zerodha_mktdata = ZerodhaMktdata(self.feed_logger)
        #self.oanda = Oanda(self.feed_logger)
        super(Mktdata, self).__init__('mktdata')

    def setup(self):
        self.feed_logger.setup()
        self.zerodha_mktdata.setup()
        # self.oanda.setup()
        register_signals(end_process)

    def run(self):
        self.feed_logger.start()
        self.zerodha_mktdata.run()
        # self.oanda.start()

        try:
            while True:
                pass
        except MktdataSignal as signal_received:
            self.close()

    def close(self):
        # self.oanda.close()
        self.zerodha_mktdata.close()
        self.feed_logger.close()

        # self.oanda.join()
        self.feed_logger.join()


def start():
    mktdata = Mktdata()
    setup_and_run(mktdata)


if __name__ == '__main__':
    start()
