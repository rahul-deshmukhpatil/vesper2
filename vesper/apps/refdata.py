import sys

from vesper.connectors.refdata_manager import get_broker_refdata_api_class
from vesper.utils.argsparser import Cmd
from vesper.utils.constants import AppType
from .app import App, setup_and_run


class RefdataApp(App):
    def __init__(self):
        App.__init__(self, AppType.ORDER_EX)
        broker_api_cls = get_broker_refdata_api_class(Cmd.parse().instance)
        self.broker_api = broker_api_cls()

    def setup(self):
        self.broker_api.setup()
        pass

    def run(self):
        return self.broker_api.run()

    def close(self):
        self.broker_api.close()
        App.close(self)


def start():
    refdata_app = RefdataApp()
    return setup_and_run(refdata_app)


if __name__ == '__main__':
    sys.exit(start())
