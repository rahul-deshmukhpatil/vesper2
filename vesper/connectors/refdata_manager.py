from vesper.connectors.refdata.fyers.api import FyersRefdataApi
from vesper.connectors.refdata.nse.api import NseRefdataApi
from vesper.connectors.refdata.zerodha.api import ZerodhaRefdataApi
from vesper.utils.constants import Broker


def get_broker_refdata_api_class(broker):
    broker_map = {Broker.FYERS: FyersRefdataApi,
                  Broker.ZERODHA: ZerodhaRefdataApi,
                  Broker.NSE: NseRefdataApi}
    return broker_map[Broker[broker.upper()]]
