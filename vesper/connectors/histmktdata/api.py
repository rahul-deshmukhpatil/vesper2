import gzip
import os
from datetime import datetime
from os import path

import pandas as pd
from enum import Enum, IntEnum

from vesper.date.date import VESPER_DATE_FMT, date2str
from vesper.histdata.utils import get_apps_hist_dir, get_hist_dir_for_date
from vesper.utils.argsparser import parser
from vesper.utils.file_utils import write_as_hdf5
from vesper.utils.log_messages import log_function_entry
from vesper.utils.loggers import logger

parser.add_argument('-d', '--histdir', required=True,
                    type=str, help="historical dir")

parser.add_argument('--from_date', required=True,
                    type=str, help="histdir from_date")

parser.add_argument('--to_date', required=True,
                    type=str, help="histdir to_date")


# parser.add_argument('--interval', required=True,
#                     type=str, help="interval [1m, 3m, 5m, 10m, 15m, 30m, 60m, "
#                                    "day]")
#
# parser.add_argument('--symbols', required=True,
#                     type=str, help="list of comma separated in format "
#                                    "NSE:SBIN,NSE:INFY")


class Intervals(Enum):
    MIN_1 = 'min_1'
    MIN_3 = 'min_3'
    MIN_5 = 'min_5'
    MIN_10 = 'min_10'
    MIN_15 = 'min_15'
    MIN_30 = 'min_30'
    MIN_60 = 'min_60'
    DAY = 'day'


FnOIntervalMap = {
    Intervals.MIN_1: 60,
    Intervals.MIN_3: 100,
    Intervals.MIN_5: 100,
    Intervals.MIN_10: 100,
    Intervals.MIN_15: 100,
    Intervals.MIN_30: 100,
    Intervals.MIN_60: 100,
    Intervals.DAY: 100}


# @TODO : remove this
class HistMktdataApi:
    def __init__(self, broker):
        self.cmd_line = parser.parse_known_args()[0]
        self.from_date = self.cmd_line.from_date
        self.to_date = self.cmd_line.to_date
        # self.symbols = self.cmd_line.symbols
        # self.interval = self.cmd_line.intervals
        self.broker = broker

        TICK_RATE = 367 * 0.1
        self.min_interval_mult = {
            Intervals.MIN_1: TICK_RATE,
            Intervals.MIN_3: TICK_RATE / 3,
            Intervals.MIN_5: TICK_RATE / 5,
            Intervals.MIN_10: TICK_RATE / 10,
            Intervals.MIN_15: TICK_RATE / 15,
            Intervals.MIN_30: TICK_RATE / 30,
            Intervals.MIN_60: TICK_RATE / 60,
            Intervals.DAY: TICK_RATE / 367,
        }
        self.min_interval_data = {}
        for interval in Intervals:
            self.min_interval_data[interval] = self.min_interval_mult[
                                                   interval] * \
                                               FnOIntervalMap[interval]

    def setup(self):
        pass

    def get_chunk_for_day(self, instr_data, date):
        chunk = []
        return chunk

    def get_file_path(self, date, exchange, symbol):
        file_dir = get_hist_dir_for_date(date, self.cmd_line,
                                         'mktdata',
                                         self.broker.name.lower())
        date = date2str(fmt=VESPER_DATE_FMT)
        return os.path.join(file_dir, f'{exchange}-{symbol}-{date}.h5')

    def chunk_and_write_data(self, interval, is_fno, exchange, symbol,
                             instr_data):
        file_path = self.get_file_path(datetime.now(), exchange, symbol)
        # empty df creats 2MB file
        if len(instr_data):
            write_as_hdf5(instr_data, file_path, f'data/ohlc/{interval.value}')

    @staticmethod
    def write_file(file_path, instr_data_for_day):
        gzip_writer = gzip.open(file_path, 'at')
        gzip_writer.write(instr_data_for_day)
        gzip_writer.close()

    def download_intervals(self, instr, exchange, symbol, is_fno):
        downloaded_data = {}
        for interval in Intervals:
            info_str = f'{exchange}-{symbol}  {interval.value}'
            try:
                instr_data = self.download_data(exchange, symbol,
                                                self.from_date,
                                                self.to_date, interval,
                                                is_fno=is_fno)

                is_illiquid = interval == Intervals.MIN_1 and len(instr_data) \
                                                          == 0
                # stock = instrument_type== EQ and exchange == segment
                is_illiquid = is_illiquid or (
                            instr.instrument_type == 'EQ' and
                            instr.segment == instr.exchange and
                            interval == Intervals.MIN_1 and
                            len(instr_data) < self.min_interval_data[interval])
                if is_illiquid:
                    logger.error(f'illiquid symbol min or no data for'
                                 f' {info_str}, ticks: {len(instr_data)} < '
                                 f'{self.min_interval_data[interval]}')
                    downloaded_data = {}
                    break
                downloaded_data[interval] = instr_data
            except Exception as ex:
                logger.error(f'failed to download_files {info_str} {ex}')

        if len(downloaded_data) == 0:
            return

        for interval in Intervals:
            if interval not in downloaded_data:
                continue
            info_str = f'{exchange}-{symbol}  {interval.value}'
            logger.info(f'downloaded files {info_str}, ticks: '
                        f'{len(downloaded_data[interval])}')
            self.chunk_and_write_data(interval, is_fno, exchange,
                                      symbol, downloaded_data[interval])

    def run_zerodha(self):
        for index, instr in self.instruments.iterrows():
            is_fno = instr.instrument_type in ['FUT', 'CE', 'PE']
            exchange, symbol = instr.exchange, instr.tradingsymbol
            self.download_intervals(instr, exchange, symbol, is_fno)

    def run(self):
        for symbol in self.symbols.replace(' ', '').split(','):
            exchange, symbol = symbol.split(':')
            instr_data = self.download_data(exchange, symbol, self.from_date,
                                            self.to_date, self.interval)

            self.chunk_and_write_data(exchange, symbol, instr_data)

    def absolute_refdata_file_path(self, file_name):
        file_name = f'{date2str(fmt=VESPER_DATE_FMT)}-{file_name}'
        file_path = path.join(
            get_apps_hist_dir(self.cmd_line), file_name)
        return file_path

    @log_function_entry
    def close(self):
        pass
