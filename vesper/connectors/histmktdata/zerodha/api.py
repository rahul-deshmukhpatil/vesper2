from collections import defaultdict, namedtuple
from datetime import datetime, timedelta
from ratelimit import limits, sleep_and_retry

import pandas as pd

from vesper.connectors.common.zerodha.zerodha_login import ZerodhaLogin
from vesper.connectors.histmktdata.api import HistMktdataApi, Intervals
from vesper.date.date import VESPER_DATETIME_FMT, VESPER_DATE_FMT, datetime2str, \
    str2datetime
from vesper.utils.constants import Broker
from vesper.utils.log_messages import log_function_entry

ZerodhaInterval = namedtuple('ZerodhaInterval', 'interval zerodha_interval '
                                                'limit_days')
IntervalMap = {Intervals.MIN_1: ZerodhaInterval(Intervals.MIN_1, 'minute', 60),
               Intervals.MIN_3: ZerodhaInterval(Intervals.MIN_3, '3minute',
                                                100),
               Intervals.MIN_5: ZerodhaInterval(Intervals.MIN_5, '5minute',
                                                100),
               Intervals.MIN_10: ZerodhaInterval(Intervals.MIN_10, '10minute',
                                                 100),
               Intervals.MIN_15: ZerodhaInterval(Intervals.MIN_15, '15minute',
                                                 100),
               Intervals.MIN_30: ZerodhaInterval(Intervals.MIN_30, '30minute',
                                                 200),
               Intervals.MIN_60: ZerodhaInterval(Intervals.MIN_60, '60minute',
                                                 400),
               Intervals.DAY: ZerodhaInterval(Intervals.DAY, 'day', 2000)}

FnOIntervalMap = {
    Intervals.MIN_1: ZerodhaInterval(Intervals.MIN_1, 'minute', 60),
    Intervals.MIN_3: ZerodhaInterval(Intervals.MIN_3, '3minute', 100),
    Intervals.MIN_5: ZerodhaInterval(Intervals.MIN_5, '5minute', 100),
    Intervals.MIN_10: ZerodhaInterval(Intervals.MIN_10, '10minute', 100),
    Intervals.MIN_15: ZerodhaInterval(Intervals.MIN_15, '15minute', 100),
    Intervals.MIN_30: ZerodhaInterval(Intervals.MIN_30, '30minute', 100),
    Intervals.MIN_60: ZerodhaInterval(Intervals.MIN_60, '60minute', 100),
    Intervals.DAY: ZerodhaInterval(Intervals.DAY, 'day', 100)}


class ZerodhaHistMktdataApi(HistMktdataApi, ZerodhaLogin):
    ONE_SECOND = 1

    def __init__(self):
        HistMktdataApi.__init__(self, Broker.ZERODHA)
        ZerodhaLogin.__init__(self)
        self.instruments = None
        self.refdata = defaultdict(dict)

    def setup(self):
        HistMktdataApi.setup(self)
        ZerodhaLogin.setup(self)
        all_instruments = self.kiteconnect.instruments()
        # all_instruments = symbols_data
        self.instruments = []
        for instr in all_instruments:
            # exclude non currency pair symbols
            if instr['exchange'] in ['CDS', 'BCD'] and \
                    'INR' not in instr['tradingsymbol']:
                continue

            expiry = instr['expiry']
            if not expiry:
                self.instruments.append(instr)
                continue

            # @TODO remove below check
            if not isinstance(expiry, str):
                time = datetime.min.time()
                expiry = datetime.combine(expiry, time)
            else:
                expiry = str2datetime(time_str=expiry, fmt='%Y-%m-%d')

            # @TODO: do not download data for very in the futures
            # they do not have any data
            if expiry and expiry - datetime.now() < timedelta(days=90):
                self.instruments.append(instr)

        self.instruments = pd.DataFrame(self.instruments)
        for index, instr in self.instruments.iterrows():
            self.refdata[instr.exchange][instr.tradingsymbol] = instr

    def run(self):
        HistMktdataApi.run_zerodha(self)

    def get_instrument_token(self, exchange, symbol):
        return self.refdata[exchange][symbol].instrument_token

    @staticmethod
    def get_normalized_interval_date(interval, from_date, to_date, is_fno):
        from_date = str2datetime(time_str=from_date, fmt=VESPER_DATE_FMT)
        to_date = str2datetime(time_str=to_date, fmt=VESPER_DATE_FMT)

        interval_map = FnOIntervalMap if is_fno else IntervalMap
        zerodha_interval = interval_map[interval].zerodha_interval
        date_limit = datetime.now() - timedelta(days=interval_map[
            interval].limit_days)
        from_date = max(date_limit, from_date)

        if to_date < date_limit:
            to_date = date_limit

        ZERODHA_HISTDATA_DATE_FMT = '%Y-%m-%d'
        from_date = datetime2str(timestamp=from_date,
                                 fmt=ZERODHA_HISTDATA_DATE_FMT) + ' 00:00:00'
        to_date = datetime2str(timestamp=to_date,
                               fmt=ZERODHA_HISTDATA_DATE_FMT) + ' 00:00:00'
        return zerodha_interval, from_date, to_date

    @sleep_and_retry
    @limits(calls=3, period=ONE_SECOND)
    def download_data(self, exchange, symbol, from_date,
                      to_date, interval, is_fno):
        instrument_token = self.get_instrument_token(exchange, symbol)
        norm_interval, norm_from_date, norm_to_date = self.get_normalized_interval_date(
            interval, from_date, to_date, is_fno)

        instr_data = self.kiteconnect.historical_data(
            instrument_token=instrument_token,
            from_date=norm_from_date,
            to_date=norm_to_date,
            interval=norm_interval,
            continuous=0, oi=int(is_fno))
        return pd.DataFrame(instr_data)

    @log_function_entry
    def close(self):
        ZerodhaLogin.close(self)
        HistMktdataApi.close(self)


symbols_data = [
    {"instrument_token": 128053508, "exchange_token": "500209",
     "tradingsymbol": "INFY", "name": "INFOSYS.", "last_price": 0.0,
     "expiry": "", "strike": 0.0, "tick_size": 0.05, "lot_size": 1,
     "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 275855110, "exchange_token": "1077559",
     "tradingsymbol": "EURINR22APRFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-04-27", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 278434566, "exchange_token": "1087635",
     "tradingsymbol": "EURINR22AUGFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-08-26", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 256503558, "exchange_token": "1001967",
     "tradingsymbol": "EURINR22DECFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-12-28", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 274297350, "exchange_token": "1071474",
     "tradingsymbol": "EURINR22FEBFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-02-24", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 277817350, "exchange_token": "1085224",
     "tradingsymbol": "EURINR22JULFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-07-27", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 277171462, "exchange_token": "1082701",
     "tradingsymbol": "EURINR22JUNFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-06-28", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 275054086, "exchange_token": "1074430",
     "tradingsymbol": "EURINR22MARFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-03-29", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 276514822, "exchange_token": "1080136",
     "tradingsymbol": "EURINR22MAYFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-05-27", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 280510214, "exchange_token": "1095743",
     "tradingsymbol": "EURINR22NOVFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-11-28", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 279745542, "exchange_token": "1092756",
     "tradingsymbol": "EURINR22OCTFUT", "name": "EURINR", "last_price": 0.0,
     "expiry": "2022-10-27", "strike": 0.0, "tick_size": 0.0025, "lot_size": 1,
     "instrument_type": "FUT", "segment": "BCD-FUT", "exchange": "BCD"},
    {"instrument_token": 128053508, "exchange_token": "500209",
     "tradingsymbol": "INFY", "name": "INFOSYS.", "last_price": 0.0,
     "expiry": "", "strike": 0.0, "tick_size": 0.05, "lot_size": 1,
     "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 128053764, "exchange_token": "500210",
     "tradingsymbol": "INGERRAND", "name": "INGERSOLL-RAND (INDIA).",
     "last_price": 0.0, "expiry": "", "strike": 0.0, "tick_size": 0.05,
     "lot_size": 1, "instrument_type": "EQ", "segment": "BSE",
     "exchange": "BSE"},
    {"instrument_token": 136173828, "exchange_token": "531929",
     "tradingsymbol": "INNOCORP", "name": "INNOCORP.", "last_price": 0.0,
     "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1,
     "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 138747652, "exchange_token": "541983",
     "tradingsymbol": "INNOVATIVE", "name": "", "last_price": 0.0, "expiry": "",
     "strike": 0.0, "tick_size": 0.01, "lot_size": 1000,
     "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 138586372, "exchange_token": "541353",
     "tradingsymbol": "INNOVATORS", "name": "", "last_price": 0.0, "expiry": "",
     "strike": 0.0, "tick_size": 0.05, "lot_size": 1600,
     "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 134103044, "exchange_token": "523840",
     "tradingsymbol": "INNOVTEC", "name": "INNOVATIVE TECH PACK.",
     "last_price": 0.0, "expiry": "", "strike": 0.0, "tick_size": 0.05,
     "lot_size": 1, "instrument_type": "EQ", "segment": "BSE",
     "exchange": "BSE"},
    {"instrument_token": 136528644, "exchange_token": "533315",
     "tradingsymbol": "INOVSYNTH", "name": "INNOVASSYNTH INVESTMENTS.",
     "last_price": 0.0, "expiry": "", "strike": 0.0, "tick_size": 0.05,
     "lot_size": 1, "instrument_type": "EQ", "segment": "BSE",
     "exchange": "BSE"},
    {"instrument_token": 136372740, "exchange_token": "532706",
     "tradingsymbol": "INOXLEISUR", "name": "INOX LEISURE.", "last_price": 0.0,
     "expiry": "", "strike": 0.0, "tick_size": 0.05, "lot_size": 1,
     "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 12379650, "exchange_token": "48358",
     "tradingsymbol": "INFY22APRFUT", "name": "INFY", "last_price": 0.0,
     "expiry": "2022-04-28", "strike": 0.0, "tick_size": 0.05, "lot_size": 300,
     "instrument_type": "FUT", "segment": "NFO-FUT", "exchange": "NFO"},
    {"instrument_token": 13308674, "exchange_token": "51987",
     "tradingsymbol": "INFY22FEBFUT", "name": "INFY", "last_price": 0.0,
     "expiry": "2022-02-24", "strike": 0.0, "tick_size": 0.05, "lot_size": 300,
     "instrument_type": "FUT", "segment": "NFO-FUT", "exchange": "NFO"},

    {"instrument_token": 240004356, "exchange_token": "937517", "tradingsymbol": "0MMFL28A", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 245241604, "exchange_token": "957975", "tradingsymbol": "0MMFSL22", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 245359108, "exchange_token": "958434", "tradingsymbol": "0MMFSL22A", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 245451780, "exchange_token": "958796", "tradingsymbol": "0MMFSL23", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 244974852, "exchange_token": "956933", "tradingsymbol": "0MUPPL24", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 239913732, "exchange_token": "937163", "tradingsymbol": "0MVAFL22", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 239914244, "exchange_token": "937165", "tradingsymbol": "0MVAFL23", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 239914756, "exchange_token": "937167", "tradingsymbol": "0MVAFL25", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 239915268, "exchange_token": "937169", "tradingsymbol": "0MVAFL27", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 243315716, "exchange_token": "950452", "tradingsymbol": "0PFC2022", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 10, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 239776260, "exchange_token": "936626", "tradingsymbol": "0PFL22B", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 239777796, "exchange_token": "936632", "tradingsymbol": "0PFL24B", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 244477700, "exchange_token": "954991", "tradingsymbol": "0PREPL28", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 10, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 249165316, "exchange_token": "973302", "tradingsymbol": "0REPL24", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 249165572, "exchange_token": "973303", "tradingsymbol": "0REPL25", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 249101060, "exchange_token": "973051", "tradingsymbol": "0RERT28", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 243986180, "exchange_token": "953071", "tradingsymbol": "0RHPL21", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 243916036, "exchange_token": "952797", "tradingsymbol": "0RHPL21A", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 243921156, "exchange_token": "952817", "tradingsymbol": "0RHPL21B", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 243933700, "exchange_token": "952866", "tradingsymbol": "0RHPL21C", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},
    {"instrument_token": 243957508, "exchange_token": "952959", "tradingsymbol": "0RHPL21D", "name": "", "last_price": 0.0,
        "expiry": "", "strike": 0.0, "tick_size": 0.01, "lot_size": 1, "instrument_type": "EQ", "segment": "BSE", "exchange": "BSE"},



]
