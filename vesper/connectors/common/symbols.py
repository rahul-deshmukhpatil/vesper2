from os import path

from vesper.date.date import datetime2str
from vesper.histdata.utils import get_hist_dir
from vesper.utils.argsparser import parser
from vesper.utils.constants import AppType, Broker
from vesper.utils.file_utils import read_csv_as_pd_df

NIFTY_50 = 'nifty50'
NIFTY_100 = 'nifty100'
NIFTY_200 = 'nifty200'
NIFTY_500 = 'nifty500'


class NewSymbols:
    def __init__(self):
        self.cmd_line = parser.parse_known_args()[0]

    def get_nifty_symbols(self, group_key):
        # @TODO : this refdata file name format should not be hardcoded
        file_name = f'{datetime2str(fmt="%Y%m%d")}-NSE_ind_{group_key}list.csv'
        dir_path = get_hist_dir(self.cmd_line,
                                app=AppType.REFDATA.name.lower(),
                                instance=Broker.NSE.name)
        symbol_file_path = path.join(dir_path, file_name)
        return read_csv_as_pd_df(symbol_file_path)
