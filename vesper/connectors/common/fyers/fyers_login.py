import webbrowser
from urllib.parse import parse_qs, urlparse

import requests
from fyers_api import accessToken
from fyers_api import fyersModel

from vesper.connectors.mktdata.fyers.custom_fyers_socket import \
    CustomFyersSocket
from vesper.utils.argsparser import parser
from vesper.utils.log_messages import log_function_entry
from vesper.utils.loggers import Logger, logger


class FyersLogin:
    AUTH_CODE = 'auth_code'

    def __init__(self):
        self.cmd_line = parser.parse_known_args()[0]
        self.Nonce = 'The value in nonce can be any random string value. This is also an optional field'
        self.scope = 'The value in scope must be openid if being passed. Though this is an optional field'
        self.state = 'optional_state'
        self.grant_type = 'authorization_code'
        self.response_type = 'code'
        self.redirect_uri = 'https://127.0.0.1:5000'
        self.secret_key = 'XWNHS8C906'
        self.client_id = 'LM1XTRVC2Q-100'

        # set during login phase
        self.fs = None
        self.fyersSocket = None
        self.fyersModel = None
        self.access_token = None

    def get_fyers_auth_token(self, response):
        logger.info(f'session generate_authcode response = {response}')
        automated_auth = True
        if automated_auth:
            # automated auth_code token
            pan = 'BRGPP8831A'
            password = 'Rahu@9760'
            username = 'XG07489'
            pin = int('9860')
            redirect_uri = 'http://127.0.0.1/loginstatus'

            data1 = f'{{"fy_id":"{username}","password":"{password}","app_id":"2","imei":"","recaptcha_token":""}}'
            requests_session = requests.Session()
            r1 = requests_session.post(
                'https://api.fyers.in/vagator/v1/login', data=data1)
            request_key = r1.json()['request_key']

            data2 = f'{{"request_key":"{request_key}","identity_type":"pin","identifier":"{pin}","recaptcha_token":""}}'
            r2 = requests_session.post(
                'https://api.fyers.in/vagator/v1/verify_pin', data=data2)
            headers = {
                'authorization': f"Bearer {r2.json()['data']['access_token']}",
                'content-type': 'application/json; charset=UTF-8'
            }

            data3 = f'{{"fyers_id":"{username}","app_id":"{self.client_id[:-4]}","redirect_uri":"{redirect_uri}","appType":"100","code_challenge":"","state":"abcdefg","scope":"","nonce":"","response_type":"code","create_cookie":true}}'
            r3 = requests_session.post(
                'https://api.fyers.in/api/v2/token', headers=headers,
                data=data3)
            parsed = urlparse(r3.json()['Url'])
            auth_code = parse_qs(parsed.query)[FyersLogin.AUTH_CODE][0]
            return auth_code
        else:
            webbrowser.open(response, new=2)
            auth_code_url = input('input auth_code from web browser : ')

            url_parts = auth_code_url.split('&')
            auth_code = None
            for part in url_parts:
                auth_code_key = f'{FyersLogin.AUTH_CODE}='
                if part.startswith():
                    auth_code = part[len(auth_code_key):]
            return auth_code

    def setup(self):
        self.set_access_token()
        self.set_fyers_model()
        self.set_fyers_socket()

    def set_access_token(self):
        session = accessToken.SessionModel(client_id=self.client_id,
                                           secret_key=self.secret_key,
                                           redirect_uri=self.redirect_uri,
                                           grant_type=self.grant_type,
                                           response_type=self.response_type)

        logger.info(f'created session = {session}')
        response = session.generate_authcode()

        auth_code = self.get_fyers_auth_token(response)

        session.set_token(auth_code)
        response = session.generate_token()
        logger.info(f'session generate_token response = {response}')
        self.access_token = response["access_token"]

    def set_fyers_model(self):
        self.fyersModel = fyersModel.FyersModel(client_id=self.client_id,
                                                token=self.access_token,
                                                log_path=Logger.create_logs_dir_path(
                                                    self.cmd_line.logsdir,
                                                    self.cmd_line.app,
                                                    self.cmd_line.instance))
        logger.info(f'generated fyersModel:{self.fyersModel}')

    def set_fyers_socket(self):
        run_background = True
        combined_access_token = f'{self.client_id}:{self.access_token}'
        self.fyersSocket = CustomFyersSocket(access_token=combined_access_token,
                                             run_background=run_background,
                                             log_path=Logger.create_logs_dir_path(
                                                 self.cmd_line.logsdir,
                                                 self.cmd_line.app,
                                                 self.cmd_line.instance)
                                             )
        logger.info(f'generated fyersSocket:{self.fyersSocket}')

    @log_function_entry
    def close(self):
        pass
