from vesper.connectors.histmktdata.zerodha.api import ZerodhaHistMktdataApi
from vesper.utils.constants import Broker


def get_broker_hist_mktdata_api_class(broker):
    broker_map = {Broker.ZERODHA: ZerodhaHistMktdataApi, }
    return broker_map[Broker[broker.upper()]]
