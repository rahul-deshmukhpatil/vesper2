from enum import Enum

fyers_refdata_file_urls = {
    # NSE – Currency Derivatives:
    'FYERS_NSE_CD.csv': 'https://public.fyers.in/sym_details/NSE_CD.csv',
    # NSE – Equity Derivatives:
    'FYERS_NSE_FO.csv': 'https://public.fyers.in/sym_details/NSE_FO.csv',
    # NSE – Capital Market:
    'FYERS_NSE_CM.csv': 'https://public.fyers.in/sym_details/NSE_CM.csv',
    # BSE – Capital Market:
    'FYERS_BSE_CM.csv': 'https://public.fyers.in/sym_details/BSE_CM.csv',
    # MCX - Commodity:
    'FYERS_MCX_COM.csv': 'https://public.fyers.in/sym_details/MCX_COM.csv',
}

# https://myapi.fyers.in/docs/#tag/Broker-Config/paths/~1Broker%20Config/patch
fyers_refdata_file_header = ['fytoken',
                             'symbol_details',
                             'exchange_instrument_type',
                             'minimum_lot_size',
                             'tick_size',
                             'isin',
                             'trading_session',
                             'last_update_date',
                             'expiry_date',
                             'symbol_ticker',
                             'exchange',
                             'segment',
                             'script_code',
                             'underlying_script_code',
                             'strike_price',
                             'option_type', ]


# https://myapi.fyers.in/docs/#tag/Appendix
class FyersExchange(Enum):
    NSE = 10  # NSE (National Stock Exchange)
    MCX = 11  # MCX (Multi Commodity Exchange)
    BSE = 12  # BSE (Bombay Stock Exchange)


# https://myapi.fyers.in/docs/#section/Segments
class FyersSegment(Enum):
    CM = 10  # Capital Market
    FO = 11  # Equity Derivatives
    CD = 12  # Currency Derivatives
    COM = 20  # Commodity Derivatives
