nse_refdata_file_urls = {
    'NSE_ind_nifty50list.csv': 'https://www1.nseindia.com/content/indices/ind_nifty50list.csv',
    'NSE_ind_nifty100list.csv': 'https://www1.nseindia.com/content/indices/ind_nifty100list.csv',
    'NSE_ind_nifty200list.csv': 'https://www1.nseindia.com/content/indices/ind_nifty200list.csv',
    'NSE_ind_nifty500list.csv': 'https://www1.nseindia.com/content/indices/ind_nifty500list.csv',
}
