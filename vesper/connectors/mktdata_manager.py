from vesper.connectors.mktdata.fyers.api import FyersMktdataApi
from vesper.connectors.mktdata.zerodha.api import ZerodhaMktdataApi
from vesper.utils.constants import Broker


def get_broker_mktdata_api_class(broker):
    broker_map = {Broker.FYERS: FyersMktdataApi,
                  Broker.ZERODHA: ZerodhaMktdataApi}
    return broker_map[Broker[broker.upper()]]
