import json
import urllib

import requests
from fyers_api.Websocket import constants
from fyers_api.Websocket.ws import FyersSocket


class CustomFyersSocket(FyersSocket):
    def __init__(self, *args, **kwargs):
        FyersSocket.__init__(self, *args, **kwargs)

    def _FyersSocket__on_message(self, ws, msg):
        if self._FyersSocket__data_type == "symbolData":
            self.response = self.parse_symbol_data(msg)
            if type(msg) == str:
                if "error" in msg:
                    msg = json.loads(msg)
                    self.response_out["s"] = msg["s"]
                    self.response_out["code"] = msg["code"]
                    self.response_out["message"] = msg["message"]
                    self.response = self.response_out
                    self.logger.error(self.response)
                    if self.websocket_data is not None:
                        self.websocket_data(self.response)
                    else:
                        print(f"Response:{self.response}")
            else:
                if self.websocket_data is not None:
                    self.websocket_data(self.response)
                else:
                    print(f"Response:{self.response}")
            # self.logger.debug(self.response)
        else:
            self.response = msg
            if type(msg) == str:
                if "error" in msg:
                    msg = json.loads(msg)
                    self.response_out["s"] = msg["s"]
                    self.response_out["code"] = msg["code"]
                    self.response_out["message"] = msg["message"]
                    self.response = self.response_out
                    self.logger.error(self.response)
                    if self.websocket_data is not None:
                        self.websocket_data(self.response)
                    else:
                        print(f"Response:{self.response}")
                else:
                    if self.websocket_data is not None:
                        self.websocket_data(self.response)
                    else:
                        print(f"Response:{self.response}")
            else:
                if self.websocket_data is not None:
                    self.websocket_data(self.response)
                else:
                    print(f"Response:{self.response}")
        # self.websocket_data()
        return

    def _FyersSocket__construct_send_message(self, symbol="", unsubscribe_flag=False):
        if unsubscribe_flag == True:
            SUB_T = 0
        else:
            SUB_T = 1
        if self._FyersSocket__data_type == constants.KEY_ORDER_UPDATE:
            message = {"T": "SUB_ORD", "SLIST": [], "SUB_T": SUB_T}
            message["SLIST"] = constants.KEY_ORDER_UPDATE
        elif self._FyersSocket__data_type == constants.KEY_DATA_UPDATE:
            message = {"T": "SUB_L2", "L2LIST": [], "SUB_T": SUB_T}
            message["L2LIST"] = symbol
            symbols = symbol
            symbols_dict = {"symbols": ""}
            symbols_dict["symbols"] = ','.join(symbols)
            url_params = urllib.parse.urlencode(symbols_dict)
            url = self.data_url + "?" + url_params
            # url = urllib.parse.unquote(url, encoding="utf-8")
            get_quotes = requests.get(url=url,
                                      headers={"Authorization": self._FyersSocket__access_token,
                                               'Content-Type': self.content})
            quotes_response = get_quotes.json()
            if quotes_response["s"] == "error":
                self.response_out["s"] = quotes_response["s"]
                self.response_out["code"] = quotes_response["code"]
                self.response_out["message"] = quotes_response["message"]
                return self.response_out
            for symbol in quotes_response["d"]:
                symbol_data = symbol["v"]["symbol"]
                fy_token = symbol["v"]["fyToken"]
                data_dict = {"fy_token": fy_token, "symbol": symbol_data}
                self.get_symtoken.append(data_dict)
        # message["SUB_T"] = int(subscribe)
        return json.dumps(message)
