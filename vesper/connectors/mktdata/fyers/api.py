from os import listdir, path

from vesper.connectors.common.fyers.fyers_login import FyersLogin
from vesper.connectors.constants.fyers import FyersExchange, \
    fyers_refdata_file_header
from vesper.connectors.mktdata.api import MktdataApi
from vesper.date.date import datetime2str
from vesper.histdata.utils import get_hist_dir
from vesper.types.types import Exchange
from vesper.utils.constants import AppType, Broker
from vesper.utils.file_utils import read_csv_as_pd_df
from vesper.utils.log_messages import log_function_entry
from vesper.utils.loggers import logger


class FyersMktdataApi(MktdataApi, FyersLogin):
    SYMBOL_DATA = 'symbolData'

    def __init__(self):
        MktdataApi.__init__(self, broker=Broker.FYERS, cons_topics=[])
        FyersLogin.__init__(self)
        self.refdata = {}
        for exchange in FyersExchange:
            self.refdata[exchange] = {}

    def setup(self):
        MktdataApi.setup(self)
        FyersLogin.setup(self)
        self.fyersSocket.websocket_data = self.on_ticks
        self.fyersSocket.on_open = self.on_socket_open
        self.fyersSocket.on_error = self.on_socket_error
        self.fyersSocket.on_close = self.on_socket_close
        self.fyersSocket.keep_running()

    def subscribe(self):
        nifty500_symbols = path.join(get_hist_dir(self.cmd_line,
                                                  app=AppType.REFDATA.name.lower(),
                                                  instance=Broker.NSE.name.lower()),
                                     f'{datetime2str(fmt="%Y%m%d")}-NSE_ind_nifty500list.csv')
        symbols = []
        for index, row in read_csv_as_pd_df(nifty500_symbols).iterrows():
            try:
                fyers_refdata = self.refdata[
                    f'{Exchange.NSE.name}:{row.Symbol}']
            except KeyError as ke:
                logger.error(
                    f'KeyError : could not subscribe to symbol '
                    f'{Exchange.NSE.name}:{row.Symbol}')
                continue

            assert (fyers_refdata.symbol_ticker ==
                    f'{Exchange.NSE.name}:{row.Symbol}-{row.Series}')
            symbols.append(fyers_refdata.symbol_ticker)

        max_subscriptions_per_web_socket = 50
        # symbols = ['MCX:CRUDEOIL22FEBFUT', 'MCX:GOLD22APRFUT']
        for start in range(0, len(symbols), max_subscriptions_per_web_socket):
            subscription_list = symbols[start: start +
                                               max_subscriptions_per_web_socket]
            logger.info(f'subscribing to {start} : {subscription_list}')
            self.fyersSocket.subscribe(symbol=subscription_list,
                                       data_type=FyersMktdataApi.SYMBOL_DATA)

    def load_refdata(self):
        fyers_ref_data_path = get_hist_dir(self.cmd_line,
                                           app=AppType.REFDATA.name.lower(),
                                           instance=Broker.FYERS.name.lower())
        for file in listdir(fyers_ref_data_path):
            if file.endswith('CM.csv'):
                refdata_df = read_csv_as_pd_df(
                    path.join(fyers_ref_data_path, file), header=None,
                    names=fyers_refdata_file_header,
                    index_col=False)
                for index, refdata in refdata_df.iterrows():
                    symbol_ticker = refdata.symbol_ticker.rsplit('-', 1)[0]
                    self.refdata[symbol_ticker] = refdata

    def on_socket_open(self):
        logger.info(f'fyers web socket opened')

    def on_socket_error(self, message):
        logger.info(f'fyers web socket error, error message: {message}')

    def on_socket_close(self, a, b):
        logger.info(f'fyers socket closed, closing message: {a} {b}')

    def run(self):
        while True:
            pass

    @log_function_entry
    def close(self):
        FyersLogin.close(self)
        MktdataApi.close(self)
