from vesper.connectors.common.zerodha.zerodha_login import ZerodhaLogin
from vesper.connectors.mktdata.api import MktdataApi
from vesper.refdata.refdata import Refdata
from vesper.refdata.symbols import Symbols, bse_etf, main_indices, nifty200, \
    nifty50, \
    nifty500, nse_etf
from vesper.types.types import Exchange
from vesper.utils.constants import Broker
from vesper.utils.log_messages import log_function_entry
from vesper.utils.loggers import logger


class ZerodhaMktdataApi(MktdataApi, ZerodhaLogin):
    def __init__(self):
        MktdataApi.__init__(self, broker=Broker.ZERODHA, cons_topics=[])
        ZerodhaLogin.__init__(self)
        self.refdata = {}

    def load_ref_data(self):
        instruments = self.kiteconnect.instruments()
        Refdata.load(instruments)

    def on_json_ticks(self, ws, json_ticks):
        self.on_ticks(json_ticks)

    def register_callbacks(self):
        self.kiteticker.on_tick = self.on_json_ticks
        self.kiteticker.on_ticks = self.on_json_ticks
        self.kiteticker.on_connect = self.on_connect
        self.kiteticker.on_reconnect = self.on_reconnect

    def subscribe_all_symbols(self):
        self.subscribe_depth(Exchange.NSE, main_indices)
        self.subscribe_depth(Exchange.BSE, bse_etf)
        self.subscribe_depth(Exchange.NSE, nse_etf)
        self.subscribe_depth(Exchange.NSE, nifty50)
        self.subscribe_depth(Exchange.NSE, nifty200)
        self.subscribe_depth(Exchange.NSE, nifty500)

        futures = Symbols.get_future_symbols(Exchange.NFO, nifty50)
        futures = futures + Symbols.get_future_symbols(Exchange.NFO, nifty200)
        self.subscribe_depth(Exchange.NFO, futures)

        cds_futures = Symbols.get_future_symbols(
            Exchange.CDS, ['USDINR', 'EURINR', 'GBPINR', 'JPYINR'], count=3)
        self.subscribe_depth(Exchange.CDS, cds_futures)

        bcd_futures = Symbols.get_future_symbols(
            Exchange.BCD, ['USDINR', 'EURINR', 'GBPINR', 'JPYINR'], count=3)
        self.subscribe_depth(Exchange.BCD, bcd_futures)

        cds_options = Symbols.get_options_symbols(
            Exchange.CDS, ['USDINR'], round(75.0), 80)
        self.subscribe_depth(Exchange.CDS, cds_options)

        bcd_options = Symbols.get_options_symbols(
            Exchange.BCD, ['USDINR'], round(75.0), 80)
        self.subscribe_depth(Exchange.BCD, bcd_options)

    def setup(self):
        MktdataApi.setup(self)
        ZerodhaLogin.setup(self)
        self.register_callbacks()
        logger.info(f'setup done for {self.__class__.__name__}')

    def load_refdata(self):
        self.load_ref_data()
        logger.info(f'loaded ref data {self.__class__.__name__}')

    def subscribe(self):
        self.subscribe_all_symbols()
        logger.info(f'subscribed to symbols {self.__class__.__name__}')

    def run(self):
        # runs into infinite loop
        # first callback on_connect subscribes instruments
        logger.info(f'running {self.__class__.__name__}')
        self.kiteticker.connect(threaded=True)
        while True:
            pass

    @log_function_entry
    def close(self):
        MktdataApi.close(self)
        ZerodhaLogin.close(self)
