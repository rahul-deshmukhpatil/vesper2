from vesper.utils.argsparser import parser
from vesper.utils.kafka_prod_cons import KafkaMessanger
from vesper.utils.log_messages import assert_over_fail, log_function_entry

parser.add_argument('-d', '--histdir', required=True,
                    type=str, help="historical dir")


class MktdataApi(KafkaMessanger):
    def __init__(self, broker, cons_topics):
        self.cmd_line = parser.parse_known_args()[0]
        KafkaMessanger.__init__(self, cons_topics)
        self.broker = broker

    def setup(self):
        KafkaMessanger.setup(self)

    @assert_over_fail
    def run(self):
        pass

    def on_ticks(self, message):
        self.publish_kafka_message(self.broker.name, message)

    @log_function_entry
    def close(self):
        KafkaMessanger.close(self)
