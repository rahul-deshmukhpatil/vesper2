from os import path

from vesper.date.date import VESPER_DATE_FMT, date2str
from vesper.histdata.utils import get_apps_hist_dir
from vesper.utils.argsparser import parser
from vesper.utils.file_utils import download_file, validate_csv_file_rows
from vesper.utils.log_messages import log_function_entry
from vesper.utils.loggers import logger

parser.add_argument('-d', '--histdir', required=True,
                    type=str, help="historical dir")


class RefdataApi:
    def __init__(self, broker):
        self.cmd_line = parser.parse_known_args()[0]
        self.broker = broker
        self.refdata_files_urls = {}
        self.minimum_rows = None

    def setup(self):
        pass

    def run(self):
        self.download_files()

    def absolute_refdata_file_path(self, file_name):
        file_name = f'{date2str(fmt=VESPER_DATE_FMT)}-{file_name}'
        file_path = path.join(
            get_apps_hist_dir(self.cmd_line), file_name)
        return file_path

    def download_files(self):
        for file_name, file_url in self.refdata_files_urls.items():
            file_path = self.absolute_refdata_file_path(file_name)
            download_file(url=file_url, target_file_path=file_path)
            validate_csv_file_rows(file_path=file_path,
                                   minimum_rows=self.minimum_rows)
            logger.info(f'downloaded file: {file_path}')
        return 0

    @log_function_entry
    def close(self):
        pass
