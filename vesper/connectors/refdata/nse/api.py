from vesper.connectors.constants.nse import nse_refdata_file_urls
from vesper.connectors.refdata.api import RefdataApi
from vesper.utils.constants import Broker


class NseRefdataApi(RefdataApi):
    def __init__(self):
        RefdataApi.__init__(self, broker=Broker.NSE)
        self.refdata_files_urls = nse_refdata_file_urls
        self.minimum_rows = 100
