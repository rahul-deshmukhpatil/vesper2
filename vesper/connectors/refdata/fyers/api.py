from vesper.connectors.constants.fyers import fyers_refdata_file_urls
from vesper.connectors.refdata.api import RefdataApi
from vesper.utils.constants import Broker


class FyersRefdataApi(RefdataApi):
    def __init__(self):
        RefdataApi.__init__(self, broker=Broker.FYERS)
        self.refdata_files_urls = fyers_refdata_file_urls
        self.minimum_rows = 100
