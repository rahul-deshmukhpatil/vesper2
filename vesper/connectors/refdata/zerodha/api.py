from vesper.connectors.common.zerodha.zerodha_login import ZerodhaLogin
from vesper.connectors.refdata.api import RefdataApi
from vesper.utils.constants import Broker
from vesper.utils.file_utils import validate_csv_file_rows
from vesper.utils.loggers import JsonFile, logger


class ZerodhaRefdataApi(RefdataApi, ZerodhaLogin):
    ZERODHA_INSTRUMENTS_FILE_NAME = 'ZERODHA-instruments.csv'

    def __init__(self):
        RefdataApi.__init__(self, broker=Broker.ZERODHA)
        ZerodhaLogin.__init__(self)
        self.minimum_rows = 100

    def setup(self):
        RefdataApi.setup(self)
        ZerodhaLogin.setup(self)

    def download_files(self):
        instruments = self.kiteconnect.instruments()
        target_file_path = self.absolute_refdata_file_path(
            self.ZERODHA_INSTRUMENTS_FILE_NAME)
        JsonFile.write(target_file_path, instruments)
        validate_csv_file_rows(file_path=target_file_path,
                               minimum_rows=self.minimum_rows)
        logger.info(f'downloaded file: {target_file_path}')

    def close(self):
        ZerodhaLogin.close(self)
        RefdataApi.close(self)
