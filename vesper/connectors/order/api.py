from enum import Enum

from vesper.utils.log_messages import assert_over_fail
from vesper.utils.kafka_prod_cons import KafkaMessanger
from vesper.utils.loggers import logger


class OrderType(Enum):
    NORMAL = 1,
    INTRADAY = 2,


class OrderUpdateType(Enum):
    INVALID = 0,
    NEW = 1,
    MODIFY = 2,
    CANCEL = 3


class OrderSide(Enum):
    INVALID = 0
    BUY = 1,
    SELL = 2


class OrderValidity(Enum):
    GFD = 1,
    IOC = 2,


class OrderApi(KafkaMessanger):
    def __init__(self, broker):
        self.broker = broker

    def setup(self):
        assert_over_fail(self, OrderApi, self.setup)

    def login(self):
        assert_over_fail(self, OrderApi, self.login)

    def send_new_order(self, order):
        assert_over_fail(self, OrderApi, self.send_new_order)

    def modify_order(self, order):
        assert_over_fail(self, OrderApi, self.modify_order)

    def cancel_order(self, order):
        assert_over_fail(self, OrderApi, self.cancel_order)

    def run_disabled(self):
        while True:
            for order_message in self.kafka_consumer:
                self.send_order_update(order_message)

    def send_order_update(self, order_message):
        if order_message.update_type == OrderUpdateType.NEW:
            self.send_new_order(order_message)
        elif order_message.update_type == OrderUpdateType.MODIFY:
            self.modify_order(order_message)
        elif order_message.update_type == OrderUpdateType.CANCEL:
            self.cancel_order(order_message)
        else:
            logger.error(f'invalid order update {order_message}')

    def close(self):
        pass
