from collections import defaultdict
import time
from io import BytesIO

import pandas as pd
from ks_api_client import ks_api

from vesper.connectors.order.api import OrderApi
from vesper.date.date import datetime2str, get_int_epoch
from vesper.trad_engine.supertrend import Supertrend
from vesper.utils.constants import Broker
from vesper.utils.geturl import get_url
from vesper.utils.loggers import logger

KOTAK_REF_DATA_URL = 'https://preferred.kotaksecurities.com/security/production/TradeApiInstruments_Cash_{}.txt'


class KotakOrderApi(OrderApi, Supertrend):
    def __init__(self):
        OrderApi.__init__(self, Broker.KOTAK)
        Supertrend.__init__(self)
        self.refdata = defaultdict(dict)
        self.access_token = '18e8bc74-8f88-33df-8563-0d7f2dca72ea'
        self.userid = 'GP31081992'
        self.consumer_key = 'WNi1YGy7DxjSbRsi3rnHhBoV0gka'
        self.ip = '127.0.0.1'
        self.app_id = 'De7faultApplication'
        self.password = 'c4FpJo4VDb'
        self.client = None

    def send_new_order(self, order_message):
        pass

    def modify_order(self, order_message):
        pass

    def cancel_order(self, order_message):
        pass

    def login(self):
        self.client = ks_api.KSTradeApi(access_token=self.access_token, userid=self.userid,
                                        consumer_key=self.consumer_key, ip=self.ip, app_id=self.app_id)
        try:
            # Login using password
            login_result = self.client.login(password=self.password)
            logger.info(f'Logged in Kotak {login_result}')
            access_code = input('Enter Access Code for Kotak from SMS : ')
            session_result = self.client.session_2fa(access_code=access_code)
            logger.info(f'generated session for Kotak {session_result}')
            logger.info(f'login successful')
        except Exception as e:
            logger.info("exception when calling SessionApi->login: %s\n" % e)

    def load_refdata(self):
        url = KOTAK_REF_DATA_URL.format(datetime2str(fmt='%d_%m_%Y'))
        refdata_csv_file = get_url(url)
        refdata_df = pd.read_csv(
            BytesIO(refdata_csv_file), delimiter='|', header=0, encoding='utf-8')
            # refdata_csv_file, delimiter='|', header=0, encoding='utf-8')
        for index, row in refdata_df.iterrows():
            self.refdata[row.exchange][row.instrumentName] = row

    def setup(self):
        self.load_refdata()
        self.login()
        Supertrend.setup(self)
        pass

    def run(self):
        while True:
            orders = self.supertrend_loop()
            sz = 5 # order limit
            for orders_chunk in (orders[pos:sz] for pos in range(0, len(orders), sz)):
                for order in orders_chunk:
                    logger.info(f'Kotak sending new order {order.symbol} {order.side} {order.quantity}')
                    instrument_token = self.refdata['NSE'][order.symbol].instrumentToken
                    self.client.place_order(order_type="MIS", instrument_token=instrument_token,
                                            transaction_type=order.side, quantity=order.quantity, price=0,
                                            disclosed_quantity=0, trigger_price=order.trigger_price,
                                            validity="GFD", variety="REGULAR", tag=order.tag)
                    if len(orders_chunk) >= sz:
                        time.sleep(1)

    def close(self):
        logout_result = self.client.logout()
        logger.info(f'logged out from Kotak {logout_result}')
        pass
