from vesper.connectors.order.kotak.api import KotakOrderApi
from vesper.utils.constants import Broker


def get_broker_order_api_class(broker):
    broker_map = {Broker.KOTAK: KotakOrderApi, }
    return broker_map[broker]
