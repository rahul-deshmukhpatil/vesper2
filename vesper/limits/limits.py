from pprint import pformat

from vesper.date.date import epoch_to_timestamp
from vesper.utils.loggers import Logger
from vesper.types.types import Side


logger = Logger.get_logger()


class Order:
    def __init__(self, leg, epoch, quantity):
        self.leg = leg
        self.epoch = epoch
        self.quantity = quantity * self.leg.ratio

        if self.quantity > 0:
            self.price = leg.md.asks[0].price
        else:
            self.price = leg.md.bids[0].price

    def __str__(self):
        timestamp = epoch_to_timestamp(self.epoch)
        return f'[ {timestamp}, {self.leg.symbol}, {self.price}, {self.quantity}]'

    def __repr__(self):
        return self.__str__()


class Position:
    def __init__(self, strategy, quantity):
        self.epoch = 0
        self.orders = []
        self.quantity = quantity

        if quantity > 0:
            self.price = strategy.mkt_bp()
        else:
            self.price = strategy.mkt_sp()

        for leg in strategy.legs:
            if leg.side == Side.BUY:
                self.orders.append(Order(leg, self.epoch, quantity))
            else:
                self.orders.append(Order(leg, self.epoch, -quantity))
            self.epoch = leg.md.epoch

    def price_paid(self):
        if self.quantity > 0:
            return self.quantity * self.price
        else:
            return -self.quantity * self.price

    def __str__(self):
        timestamp = epoch_to_timestamp(self.epoch)
        orders_str = pformat(self.orders)
        return f'[ {timestamp}, {self.price}, {abs(self.quantity)}, {orders_str}]'

    def __repr__(self):
        return self.__str__()


class Limit:
    def __init__(self, max_quantity, min_quantity):
        assert(max_quantity > min_quantity)
        self.max_quantity = max_quantity
        self.min_quantity = min_quantity

    def net_position(self):
        assert False

    def can_buy(self, quantity):
        return self.net_position() + quantity < self.max_quantity

    def can_sell(self, quantity):
        return self.net_position() - quantity > self.min_quantity
