import gzip
import json
from datetime import datetime, timedelta
from os.path import isfile

from vesper.date.date import str2datetime
from vesper.mktdata.feed_logger import FeedLogger
from vesper.utils.argsparser import Cmd
from vesper.utils.loggers import Logger
from vesper.utils.refdata_utils import load_hist_refdata


class Backtester:
    @staticmethod
    def add_common_fields():
        Cmd.add_argument('--histdir', required=True, type=str,
                         help="Historical mktdata dir in format has data "
                              "yyyymmdd/mktdata/mktdata")
        Cmd.add_argument('--fromdate', required=False,
                         type=str, help="from fromdate")
        Cmd.add_argument('--days', required=False, type=int,
                         default=1, help="till fromdate")

    add_common_fields.__func__()

    def __init__(self, feed):
        self.feed = feed
        self.fromdate = Cmd.parse().from_date
        self.histdir = Cmd.parse().histdir
        self.days = Cmd.parse().days
        if self.fromdate is None:
            self.fromdate = datetime.now() - timedelta(self.days)
        else:
            self.fromdate = str2datetime(self.fromdate, '%Y%m%d')

    def setup(self):
        load_hist_refdata(self.histdir, self.fromdate, 'mktdata', 'mktdata')

    def run(self):
        for day in range(self.days):
            current_date = self.fromdate + timedelta(day)
            hist_dir = Logger.get_app_logs_dir(
                self.histdir, current_date, 'mktdata', 'mktdata')
            feed_file = FeedLogger.get_feed_file(hist_dir)
            if isfile(feed_file):
                self.publish(feed_file)

    def publish(self, feed_file):
        with gzip.open(feed_file, 'r') as ff:
            json_line = ff.readline()
            while json_line:
                ticks = json.loads(json_line)
                self.feed.on_ticks(ticks)
                json_line = ff.readline()

    def close(self):
        pass
