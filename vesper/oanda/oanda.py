from datetime import datetime

import v20 as v20
from v20 import pricing

from vesper.multiprocessing.thread import VesperThread
from vesper.types.types import Exchange
from vesper.utils.loggers import Logger, RefDataLogger
from vesper.refdata.refdata import Refdata
from vesper.utils.exception import VesperException
from vesper.utils.vesper_signals import register_signals

logger = Logger.get_logger()


class OandaMktdataSignal(VesperException):
    def __init__(self, signal_num):
        super(OandaMktdataSignal, self).__init__(
            f'received signal : {signal_num}')


def end_process(signal_num, tb_frames):
    error = f'signal received {signal_num}'
    raise OandaMktdataSignal(error).with_traceback(None)


oandaV20Config = {'hostname': 'api-fxtrade.oanda.com', 'streaming_hostname': 'stream-fxtrade.oanda.com', 'port': '443', 'ssl': 'true', 'token': '1c35db2af9c894cc4adb2911d7f7ad65-b8f0c2400c8b8d9577af597e00271716', 'username': 'rahuldeshmukhpatil', 'datetime_format': 'UNIX', 'accounts': ['001-004-2739815-001', '- 001-004-2739815-002'], 'active_account': '001-004-2739815-001'
                  }


class Oanda(VesperThread):
    def __init__(self, feed_logger):
        super(Oanda, self).__init__()
        self.symbols = None
        self.subscriptions = None
        self.oanda_api_ctx = None
        self.loggedIn = False
        self.mktdata_queue = feed_logger.mktdata_queue
        self.instrument_ids = {}

    def login_oanda(self):
        # Initialise
        """"
        Initialize an API context based on the Config instance
        """
        self.oanda_api_ctx = v20.Context(
            oandaV20Config['streaming_hostname'],
            oandaV20Config['port'],
            oandaV20Config['ssl'],
            application='currencyfeed',
            token=oandaV20Config['token'],
            datetime_format=oandaV20Config['datetime_format'])

        self.loggedIn = True
        logger.info('logged in Oanda')

    def load_ref_data(self):
        instruments = Oanda.generate_ref_data()
        RefDataLogger.write(instruments)
        Refdata.load(instruments)

        for instrument in instruments:
            self.instrument_ids[instrument['tradingsymbol']
                                ] = instrument['instrument_token']

    def subscribe(self):
        cds_spot_fx_currencies = ['USD_INR', 'EUR_USD',
                                  'GBP_USD', 'EUR_INR', 'USD_JPY', 'USD_CHF']
        self.subscriptions = cds_spot_fx_currencies

    def setup(self):
        register_signals(end_process)
        self.login_oanda()
        self.load_ref_data()
        self.subscribe()
        self.setup_done = True
        logger.info(f'setup done for {self.__class__.__name__}')

    def create_pricing_stream(self):
        pricing_stream_ctx = self.oanda_api_ctx.pricing.stream(
            oandaV20Config['active_account'],
            instruments=",".join(self.subscriptions),
        )
        return pricing_stream_ctx

    def run(self):
        # runs into infinite loop
        # first callback on_connect subscribes instruments
        logger.info(f'running {self.__class__.__name__}')
        pricing_stream = self.create_pricing_stream()

        for msg_type, msg in pricing_stream.parts():
            if isinstance(msg, pricing.ClientPrice):
                try:
                    tick = self.zerodha_update(msg)
                    self.mktdata_queue.put([tick])
                except:
                    pass
            if not self.running:
                break
        logger.info(f'stopping {self.__class__.__name__}')

    def close(self):
        super(Oanda, self).close()
        self.mktdata_queue = None
        logger.info(f'closed {self.__class__.__name__}')

    def zerodha_update(self, client_price):
        fx_pair = client_price.instrument[0:3] + client_price.instrument[4:7]
        ctime = datetime.fromtimestamp(int(float(client_price.time)))
        current_time = ctime.strftime('%Y-%m-%d %H:%M:%S')
        update = {}

        update["timestamp"] = current_time
        update["exchange"] = Exchange.OANDA.name
        update["instrument_token"] = self.instrument_ids[fx_pair]
        update["tradingsymbol"] = fx_pair
        update["change"] = 0
        update["oi"] = 0
        update["average_price"] = 0.0
        update["ohlc"] = {"high": 0, "close": 0, "open": 0, "low": 0}
        update["tradable"] = "true" if client_price.tradeable else "false"

        buy_levels = []
        for index, bid in enumerate(client_price.bids):
            data = {'price': bid.price, 'quantity': bid.liquidity, 'orders': 1}
            buy_levels.append(data)

        sell_levels = []
        for index, ask in enumerate(client_price.asks):
            data = {'price': ask.price, 'quantity': ask.liquidity, 'orders': 1}
            sell_levels.append(data)

        update["depth"] = {"buy": buy_levels, "sell": sell_levels}

        update["last_price"] = None
        update["last_quantity"] = 1
        update["mode"] = "full"
        update["last_trade_time"] = None
        update["buy_quantity"] = 0
        update["sell_quantity"] = 0
        update["oi_day_high"] = 0

        return update

    @staticmethod
    def generate_ref_data():
        instruments = [{"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                        0.0, "expiry": "", "exchange_mktdata_symbol": "USD_INR",  "tradingsymbol": "USDINR", "lot_size": 1, "instrument_token": 1000000001,
                        "strike": 0.0, "segment": "CDS", "exchange_token": "1000000001"}, {"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                                                                                           0.0, "expiry": "", "exchange_mktdata_symbol": "EUR_INR",  "tradingsymbol": "EURINR", "lot_size": 1, "instrument_token": 1000000002,
                                                                                           "strike": 0.0, "segment": "CDS", "exchange_token": "1000000002"},
                       {"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                        0.0, "expiry": "", "exchange_mktdata_symbol": "GBP_INR",  "tradingsymbol": "GBPINR", "lot_size": 1, "instrument_token": 1000000003,
                        "strike": 0.0, "segment": "CDS", "exchange_token": "1000000003"},
                       {"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                        0.0, "expiry": "", "exchange_mktdata_symbol": "EUR_CHF",  "tradingsymbol": "EURCHF", "lot_size": 1, "instrument_token": 1000000004,
                        "strike": 0.0, "segment": "CDS", "exchange_token": "1000000004"},
                       {"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                        0.0, "expiry": "", "exchange_mktdata_symbol": "EUR_USD",  "tradingsymbol": "EURUSD", "lot_size": 1, "instrument_token": 1000000005,
                        "strike": 0.0, "segment": "CDS", "exchange_token": "1000000005"},
                       {"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                        0.0, "expiry": "", "exchange_mktdata_symbol": "GBP_USD",  "tradingsymbol": "GBPUSD", "lot_size": 1, "instrument_token": 1000000006,
                        "strike": 0.0, "segment": "CDS", "exchange_token": "1000000006"},
                       {"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                        0.0, "expiry": "", "exchange_mktdata_symbol": "JPY_USD",  "tradingsymbol": "JPYUSD", "lot_size": 1, "instrument_token": 1000000007,
                        "strike": 0.0, "segment": "CDS", "exchange_token": "910000000007"},
                       {"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                        1.0, "expiry": "", "exchange_mktdata_symbol": "INR_JPY",  "tradingsymbol": "INRJPY", "lot_size": 1, "instrument_token": 1000000008,
                        "strike": 0.0, "segment": "CDS", "exchange_token": "1000000008"},
                       {"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                        0.0, "expiry": "", "exchange_mktdata_symbol": "USD_JPY",  "tradingsymbol": "USDJPY", "lot_size": 1, "instrument_token": 1000000009,
                        "strike": 0.0, "segment": "CDS", "exchange_token": "1000000009"},
                       {"instrument_type": "EQ", "tick_size": 0.0025, "name": "", "exchange": "OANDA", "last_price":
                        0.0, "expiry": "", "exchange_mktdata_symbol": "USD_CHF", "tradingsymbol": "USDCHF",
                        "lot_size": 1, "instrument_token": 1000000010,
                        "strike": 0.0, "segment": "CDS", "exchange_token": "1000000010"}
                       ]

        return instruments
