from json import loads

from kafka import KafkaConsumer

from vesper.backtester.backtester import Backtester
from vesper.utils.loggers import Logger
from vesper.strategy.books import get_book
from vesper.utils.argsparser import Cmd

logger = Logger.get_logger()


def add_common_fields():
    Cmd.add_argument('--live', required=False, type=bool,
                     default=False, help="To run live")
    Cmd.add_argument('--topic', required=False, type=str,
                     default='test-raw-feed', help="topic to publish on")


class CoreFeed:
    add_common_fields()

    def __init__(self, feed):
        self.feed = feed
        self.subscriptions = {}
        self.live = Cmd.parse().live
        self.topic = Cmd.parse().topic
        self.bt = Backtester(self)
        self.kafka_consumer = None

    def setup(self):
        # backtester will load refdata
        self.bt.setup()
        if self.live:
            self.kafka_consumer = self.get_kafka_consumer()

    def get_kafka_consumer(self):
        return KafkaConsumer(
            self.topic,
            bootstrap_servers=['localhost:9092'],
            auto_offset_reset='latest',
            enable_auto_commit=True,
            group_id='my-group',
            value_deserializer=lambda x: loads(x.decode('utf-8')))

    def run(self):
        if self.live:
            for message in self.kafka_consumer:
                self.feed.on_ticks(message.value)
        else:
            self.bt.run()

    # backtester ticks
    def on_ticks(self, ticks):
        self.feed.on_ticks(ticks)

    def close(self):
        if self.bt:
            self.bt.close()


class Feed(CoreFeed):
    def __init__(self, trad_engine):
        CoreFeed.__init__(self, self)
        self.trad_engine = trad_engine
        self.subscriptions = {}

    def subscribe(self, subscriptions):
        for instrument_id, exchange, symbol in subscriptions:
            self.subscriptions[instrument_id] = get_book(exchange, symbol)

    def on_ticks(self, ticks):
        callback = False
        for tick in ticks:
            instrument_id = tick['instrument_token']
            if instrument_id in self.subscriptions:
                self.subscriptions[instrument_id].update_zerodha_tick(tick)
                callback = True

        if callback:
            self.trad_engine.on_data_updated()

    def close(self):
        CoreFeed.close(self)
