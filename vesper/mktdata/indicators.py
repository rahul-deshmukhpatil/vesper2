import talib
import pandas as pd
from vesper.types.types import Interval


class Indicator:
    @staticmethod
    def create_indicator(talib_func, timeperiod, *args, **kwargs):
        def wrapper(data, *local_args, **local_kwargs):
            new_args = []
            for arg in local_args:
                if type(arg) is pd.Series:
                    nparray = data[-timeperiod * 2:].to_numpy()
                    new_args.append(nparray)
                else:
                    new_args.append(arg)

            return talib_func(data[-timeperiod * 2:].to_numpy(), *tuple(new_args), *args, timeperiod=timeperiod, **local_kwargs, **kwargs)

        return wrapper

    def __init__(self, name, frame_interval, timeperiod, *args, **kwargs):
        if hasattr(talib, name):
            talib_func = getattr(talib, name)
        else:
            assert False

        assert (type(frame_interval) is Interval)

        self.frame_interval = frame_interval
        self.timeperiod = timeperiod
        self.name = name + str(frame_interval) + str(timeperiod)
        self.get = Indicator.create_indicator(talib_func,
                                              timeperiod * frame_interval,
                                              *args,
                                              **kwargs)
