from vesper.utils.loggers import Logger

logger = Logger.get_logger()


class FeedHandler:

    def __init__(self, mktdata_queue):
        FeedHandler.instance = self
        feed_file_dir = Logger.create_logs_dir_path()
        FeedHandler.mktdata_queue = mktdata_queue

    @staticmethod
    def close():
        logger.info('closing the feed handler')
