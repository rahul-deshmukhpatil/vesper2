import gzip
import json
from queue import Queue, Empty

from kafka import KafkaProducer

from vesper.multiprocessing.thread import VesperThread
from vesper.utils import jsonutils
from vesper.utils.loggers import Logger

logger = Logger.get_logger()


class FeedLogger(VesperThread):
    @staticmethod
    def get_feed_file(feed_file_dir):
        return f'{feed_file_dir}/feed.gz'

    def __init__(self):
        super(FeedLogger, self).__init__()
        feed_file_dir = Logger.create_logs_dir_path()
        feed_file = FeedLogger.get_feed_file(feed_file_dir)
        self.feed_logger = gzip.open(feed_file, 'at')
        self.mktdata_queue = Queue()
        self.kafka_publisher = None

    def setup(self):
        self.setup_done = True
        self.kafka_publisher = KafkaProducer(
            bootstrap_servers=['localhost:9092'])
        logger.info(f'setup done for {self.__class__.__name__}')

    def run(self):
        logger.info(f'running {self.__class__.__name__}')
        while self.running:
            try:
                ticks = self.mktdata_queue.get(block=True, timeout=1)
                json_str = json.dumps(
                    ticks, default=jsonutils.custom_datetime_encoder) + '\n'
                self.kafka_publisher.send(
                    'raw-feed', value=json_str.encode('utf-8'))
                self.feed_logger.write(json_str)
            except Empty as e:
                pass
        logger.info(f'stopping {self.__class__.__name__}')

    def close(self):
        logger.info('closing the feed file')
        self.feed_logger.flush()
        self.feed_logger.close()
        super(FeedLogger, self).close()
        logger.info(f'closed {self.__class__.__name__}')
