from vesper.date.date import str2datetime

from pandas import pandas as pd

from vesper.types.types import TickMessage


class PriceVolumeData:
    def __init__(self, epoch, price):
        self.type = TickMessage.PV
        self.epoch = epoch
        self.open = price
        self.close = price
        self.high = price
        self.low = price
        self.volume = 0.0

        self.value = 0.0
        self.avg_price = 0.0
        self.positive_change = 0.0
        self.negative_change = 0.0
        self.buy_ticks = 0.0
        self.buy_volume = 0.0
        self.buy_value = 0.0
        self.sell_ticks = 0.0
        self.sell_volume = 0.0
        self.sell_value = 0.0

    def update_high_low(self, price):
        if price > self.high:
            self.high = price
        elif price < self.low:
            self.low = price

    def update_price_change(self, price, volume, price_volume):
        is_buy_tick = False
        if price > self.close:
            is_buy_tick = True
        elif price == self.close and price > self.open:
            is_buy_tick = True

        if is_buy_tick:
            self.buy_ticks += 1
            self.buy_volume += volume
            self.positive_change += price - self.close
            self.buy_value += price_volume
        else:
            self.sell_ticks += 1
            self.sell_volume += volume
            self.negative_change += self.close - price
            self.sell_value += price_volume

    def update(self, price, volume):
        price_volume = price * volume

        self.update_high_low(price)
        self.update_price_change(price, volume, price_volume)

        self.close = price
        self.volume += volume
        self.value += price_volume

        try:
            self.avg_price = self.value / self.volume
        except ZeroDivisionError:
            pass


class PriceVolume:
    def __init__(self):
        self.df = pd.DataFrame({}, columns=list(PriceVolumeData(0, 0).__dict__.keys()))
        self.pv_data = PriceVolumeData(0, 0.0)

    def update(self, epoch, price, volume):
        new_epoch = int(epoch / 60) * 60
        if self.pv_data.epoch != new_epoch:
            self.pv_data = PriceVolumeData(new_epoch, price)
            self.df = self.df.append(self.pv_data.__dict__, ignore_index=True)

        self.pv_data.update(price, volume)
        self.df.iloc[-1] = list(self.pv_data.__dict__.values())


class Level:
    def __init__(self):
        self.price = 0.0
        self.quantity = 0

    def update(self, price, quantity):
        self.price = float(price)
        self.quantity = int(quantity)


class ExchangeSymbol:
    def __init__(self, exchange, symbol):
        self.exchange = exchange
        self.symbol = symbol


class Instrument:
    BOOK_LEVELS = 5

    def __bool__(self):
        if self.tradable and self.bids[0].quantity and self.asks[0].quantity:
            return True
        else:
            return False

    @staticmethod
    def get_exchange_symbol(exchange, symbol):
        return str(exchange) + '::' + symbol

    def __init__(self, exchange, symbol):
        self.exchange = exchange
        self.symbol = symbol
        self.exchange_symbol = Instrument.get_exchange_symbol(exchange, symbol)

        self.type = TickMessage.DEPTH
        self.epoch = 0
        self.volume = 0
        self.tradable = False
        self.ltp = 0.0
        self.ltq = 0
        self.buyQty = None
        self.sellQty = None
        self.bids = []
        self.asks = []
        self.pv = PriceVolume()

        for i in range(Instrument.BOOK_LEVELS):
            self.bids.append(Level())
            self.asks.append(Level())

    def update_zerodha_tick(self, tick):
        self.epoch = str2datetime(tick['timestamp'], '%Y-%m-%d %H:%M:%S').timestamp()

        if self.epoch:
            self.tradable = tick['tradable']
            self.fill_trade(tick)
            self.fill_book(tick)

            if self.tradable:
                mid_price = (self.bids[0].price + self.asks[0].price) / 2
                self.pv.update(self.epoch, mid_price, self.ltq)

    def fill_trade(self, tick):
        volume = tick['volume']

        if volume != self.volume:
            self.ltq = volume - self.volume
            self.ltp = tick['last_price']
            self.volume = volume

    def fill_book(self, tick):
        depth = tick['depth']
        bids = depth['buy']
        asks = depth['sell']
        self.buyQty = tick['buy_quantity']
        self.sellQty = tick['sell_quantity']

        self.update_levels(self.bids, bids)
        self.update_levels(self.asks, asks)

    @staticmethod
    def update_levels(levels, depth):
        for i in range(Instrument.BOOK_LEVELS):
            if i < len(depth):
                levels[i].update(depth[i]['price'], depth[i]['quantity'])
            else:
                levels[i].update(0.0, 0)

    def bp(self):
        return self.asks[0].price

    def sp(self):
        return self.bids[0].price
