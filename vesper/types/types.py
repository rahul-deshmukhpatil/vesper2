from enum import Enum, auto, IntEnum


class TickMessage(IntEnum):
    DEPTH = auto()
    PV = auto()


class Exchange(Enum):
    NSE = auto()
    NFO = auto()
    MCX = auto()
    CDS = auto()
    BSE = auto()
    BCD = auto()
    OANDA = auto()


class Side(Enum):
    BUY = auto()
    SELL = auto()


class Interval(IntEnum):
    MIN1 = 1
    MIN2 = MIN1 * 2
    MIN3 = MIN1 * 3
    MIN5 = MIN1 * 5
    MIN10 = MIN1 * 10
    MIN15 = MIN1 * 15
    HALF_HOUR = MIN1 * 30
    HOUR = MIN1 * 60
