from pprint import pformat

from vesper.date.date import epoch_to_timestamp
from vesper.limits.limits import Limit, Order
from vesper.utils.loggers import Logger
from vesper.types.types import Side


logger = Logger.get_logger()


class Position:
    def __init__(self, strategy, quantity):
        self.epoch = 0
        self.orders = []
        self.quantity = quantity

        if quantity > 0:
            self.price = strategy.mkt_bp()
        else:
            self.price = strategy.mkt_sp()

        for leg in strategy.legs:
            if leg.side == Side.BUY:
                self.orders.append(Order(leg, self.epoch, quantity))
            else:
                self.orders.append(Order(leg, self.epoch, -quantity))
            self.epoch = leg.md.epoch

    def price_paid(self):
        if self.quantity > 0:
            return self.quantity * self.price
        else:
            return -self.quantity * self.price

    def __str__(self):
        timestamp = epoch_to_timestamp(self.epoch)
        orders_str = pformat(self.orders)
        return f'[ {timestamp}, {self.price}, {abs(self.quantity)}, {orders_str}]'

    def __repr__(self):
        return self.__str__()


class PositionManager(Limit):
    def __init__(self, max_quantity, min_quantity):
        Limit.__init__(self, max_quantity, min_quantity)
        self.positions = []

    def take_position(self, quantity):
        position = Position(self, quantity)
        self.positions.append(position)
        logger.info(f'taking position : {position}')

    def buy(self, quantity):
        if self.can_buy(quantity):
            self.take_position(quantity)

    def sell(self, quantity):
        if self.can_sell(quantity):
            self.take_position(-quantity)

    def net_position(self):
        result = 0
        for position in self.positions:
            result += position.quantity
        return result

    def profit(self):
        price_paid = 0
        for position in self.positions:
            price_paid += position.price_paid()

        return -price_paid

    def counter_position_value(self):
        net_position = self.net_position()
        if net_position:
            counter_position = Position(self, -net_position)
            return counter_position.price_paid()
        else:
            return 0

    def current_profit(self):
        profit = self.profit()
        return profit + self.counter_position_value()

    def price_for_buying(self):
        pass

    def price_for_selling(self):
        pass

    def modify_pricing_orders(self):
        self.price_for_selling()
        self.price_for_buying()
