import sys

from kiteconnect import KiteTicker
from kiteconnect import KiteConnect

from .request_token import ZerodhaSelenium
from vesper.utils import exception
from vesper.utils.loggers import logger
from vesper.refdata.symbols import Symbols
from vesper.utils.loggers import Logger

pool = {
    'pool_connections': 10,
    'pool_maxsize': 10,
    'max_retries': 0,
    'pool_block': False
}

logger = Logger.get_logger()


class Zerodha:
    ws = None
    subscriptions = []

    def __init__(self):
        self.apikey = 'jb6sff8xaht3horv'
        self.secretkey = '3lk3dl6izlpqh6ziq1rg2rrc85u935lz'
        self.access_token = None
        self.kiteconnect = None
        self.kiteticker = None
        self.loggedIn = False

    def login_zerodha(self):
        logger.info('Logging in to zerodha')
        # Initialise
        self.login_kiteconnect()
        self.login_kiteticker()
        self.loggedIn = True

    def login_kiteconnect(self):
        logger.info('logging in KiteConnect')
        self.kiteconnect = KiteConnect(api_key=self.apikey, pool=pool)
        self.generate_and_set_access_token()
        logger.info('Logged to KiteConnect')

    def login_kiteticker(self):
        logger.info('logging in kiteticker')

        logger.info('Creating kite ticker with api_key: %s, access_token: %s',
                    self.apikey, self.access_token)
        self.kiteticker = KiteTicker(self.apikey,
                                     self.access_token,
                                     reconnect=True,
                                     debug=True,
                                     reconnect_max_tries=300,
                                     reconnect_max_delay=5,
                                     connect_timeout=5)

        logger.info('logged in kiteticker')

    @staticmethod
    def print_login_details(data):
        logger.info('Logged in to the zerodha system : ')
        logger.info('Exchanges	: %s', data['exchanges'])
        logger.info('products	: %s', data['products'])
        logger.info('Order Types: %s', data['order_types'])

    def generate_and_set_access_token(self):
        try:
            logger.info(f'logging in to zerodha via selenium')
            zerodha_login_page = ZerodhaSelenium()
            logger.info(f'actual logging in to zerodha via selenium')
            request_token = zerodha_login_page.login_and_get_token()
            logger.info(
                f'generating zerodha session with requestToken {request_token}')
            data = self.kiteconnect.generate_session(
                request_token, self.secretkey)
            Zerodha.print_login_details(data)
            self.access_token = data['access_token']
            logger.info('myAccessToken is %s', self.access_token)
        except:
            type, value, traceback = sys.exc_info()
            logger.info(
                f'Could not login to zerodha : {type} {value.filename}, {value.strerror}')
            raise exception.CouldntLoginToZerodha()

        self.kiteconnect.set_access_token(self.access_token)

    @staticmethod
    def subscribe(exchange, symbols):
        for symbol in symbols:
            # if symbol in Symbols.all_symbols[exchange].keys():
            try:
                Zerodha.subscriptions.append(
                    Symbols.all_symbols[exchange][symbol].instrument_id)
                logger.info(f'subscribing {exchange}:{symbol}')
            except KeyError as key:
                logger.info(
                    f'failed to subscribe {exchange}:{symbol} as did not find symbol in all_symbols')

        if Zerodha.ws is not None and len(Zerodha.subscriptions) > 0:
            logger.info(f'sending subscriptions to zerodha')
            Zerodha.ws.subscribe(Zerodha.subscriptions)
            Zerodha.ws.set_mode(Zerodha.ws.MODE_FULL, Zerodha.subscriptions)

    @staticmethod
    def on_connect(ws, response):
        logger.info(f'KiteTicker on_connect : {str(response)}')
        Zerodha.ws = ws
        Zerodha.subscribe('', [])

    @staticmethod
    def on_reconnect(ws, response):
        logger.info(f'KiteTicker on_reconnect : {str(response)}')
        Zerodha.ws = ws
        Zerodha.subscribe('', [])

    @staticmethod
    def on_close(ws, code, reason):
        # On connection close stop the main loop
        ws = None
        logger.error(f'Closing the zerodha connection, reason {reason}')

    @staticmethod
    def on_error(ws, code, reason):
        logger.error(f'Error in zerodha connection, reason {reason}')

    def close(self):
        if hasattr(self.kiteticker, 'ws') and self.kiteticker.ws is not None and self.kiteticker.is_connected():
            self.kiteticker.close()
