import time

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


# used to work but stopped on going ahead after login at validating 2FA
class ZerodhaSelenium(object):

    def __init__(self):
        self.timeout = 5
        self.username = 'DG2081'
        self.password = '1Jun2019'
        self.pin = '986008'
        chrome_driver_manager = ChromeDriverManager()
        installed = chrome_driver_manager.install()
        self.driver = webdriver.Chrome(installed)

    def getCssElement(self, cssSelector):
        '''
        To make sure we wait till the element appears
        '''
        return WebDriverWait(self.driver, self.timeout).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, cssSelector)))

    def login_and_get_token(self):
        # let's login
        self.driver.get(
            "https://kite.trade/connect/login?api_key=yabfgbz64qlv3caj&v=3")
        try:
            userNameField = self.getCssElement("input[placeholder='User ID']")
            userNameField.send_keys(self.username)
            passwordField = self.getCssElement("input[placeholder=Password]")
            passwordField.send_keys(self.password)

            loginButton = self.getCssElement("button[type=submit]")
            loginButton.click()

            # 2FA
            form2FA = self.getCssElement("form.twofa-form")
            pinField = self.getCssElement("input[label='PIN']")
            pinField.send_keys(self.pin)

            buttonSubmit = self.getCssElement("button[type=submit]")
            buttonSubmit.click()

        except TimeoutException:
            print("Timeout occurred")

        # close chrome
        time.sleep(2)
        url = self.driver.current_url
        self.driver.close()
        return url.split('request_token=')[1][:32]


if __name__ == "__main__":
    obj = ZerodhaSelenium()
    request_token = obj.login_and_get_token()
    print(f'request_token is : {request_token}')
