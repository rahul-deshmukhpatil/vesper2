import signal
from .exception import SignalReceived


def end_process(signal_num, tb_frames):
    error = f'signal received {signal_num}'
    raise SignalReceived(error).with_traceback(None)


def register_signals(callback):
    signal.signal(signal.SIGINT, callback)
    signal.signal(signal.SIGTERM, callback)
