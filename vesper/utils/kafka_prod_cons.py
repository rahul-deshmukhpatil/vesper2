import json
from json import loads

from kafka import KafkaConsumer, KafkaProducer

from vesper.utils import jsonutils
from vesper.utils.loggers import logger


class KafkaMessanger:
    def __init__(self, cons_topics):
        self.kafka_publisher = None
        self.kafka_consumer = {}

        if isinstance(cons_topics, str):
            cons_topics = [cons_topics]
        self.cons_topics = cons_topics

    def setup(self):
        self.kafka_publisher = KafkaProducer(
            bootstrap_servers=['localhost:9092'])

        self.kafka_consumer = KafkaConsumer(
            *self.cons_topics,
            bootstrap_servers=['localhost:9092'],
            auto_offset_reset='latest',
            consumer_timeout_ms=1000,
            enable_auto_commit=True,
            group_id='my-group',
            value_deserializer=lambda x: loads(x.decode('utf-8')))

    def publish_kafka_message(self, topic, message):
        logger.info(f'publishing data on topic {topic}')
        json_str = json.dumps(
            message, default=jsonutils.custom_datetime_encoder) + '\n'
        self.kafka_publisher.send(topic, value=json_str.encode('utf-8'))

    def get_kafka_messages(self):
        messages = []
        for message in self.kafka_consumer:
            messages.append(message)
        return messages

    def close(self):
        pass
