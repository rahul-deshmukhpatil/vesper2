#!/usr/bin/python
import urllib.error
import urllib.parse
import urllib.request
from urllib.error import URLError


def text(elt):
    return elt.text_content().replace('\xa0', ' ')


def get_url(url, try_count=1):
    if not url:
        print('URL provided to get_url is empty !!!')
        exit(-1)

    hdr = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
        'Accept-Encoding': 'none',
        'Accept-Language': 'en-US,en;q=0.8',
        'Connection': 'keep-alive'}

    req = urllib.request.Request(url, headers=hdr)

    while try_count:
        try_count -= 1
        try:
            response = urllib.request.urlopen(req)
            return response.read()
        except URLError as e:
            if hasattr(e, 'reason'):
                print(('We failed to reach a server. url [%s]' % (url)))
                print(('Reason: ', e.reason))
                raise e
            elif hasattr(e, 'code'):
                print(('The server couldn\'t fulfill the request. url [%s]' % (url)))
                print(('Error code: ', e.code))
            continue
