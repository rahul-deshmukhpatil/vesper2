class VesperException(Exception):
    def __init__(self, error):
        super(VesperException, self).__init__(error)


class EmptyFile(VesperException):
    def __init__(self, file):
        super(EmptyFile, self).__init__(f'file is empty: {file}')


class CannotReadWriteFile(VesperException):
    def __init__(self, file):
        super(CannotReadWriteFile, self).__init__(
            f'could not create file or dir for read/write: {file}')


class CouldntLoginToZerodha(VesperException):
    def __init__(self):
        super(CouldntLoginToZerodha, self).__init__(
            f'could not login to zerodha, check if request token is valid')


class SignalReceived(VesperException):
    def __init__(self, signal_num):
        super(SignalReceived, self).__init__(f'received signal : {signal_num}')
