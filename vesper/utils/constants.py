from enum import Enum


class AppType(Enum):
    INVALID_APP = 0
    TEST_APP = 1
    MKTDATA = 2,
    ORDER_EX = 3,
    REFDATA = 4,
    TRADING_ENGINE = 5,
    FEED_LOGGER = 6,
    HIST_MKTDATA = 2,


class Broker(Enum):
    INVALID_BROKER = 0
    KOTAK = 1
    ZERODHA = 2
    FYERS = 3
    NSE = 4
    NSE_POST_CLOSE = 4
