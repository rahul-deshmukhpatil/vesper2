import json
import logging
import sys
from logging import DEBUG, FileHandler, Formatter, getLogger

from vesper.date.date import datetime2str
from vesper.utils import jsonutils
from vesper.utils.argsparser import parser
from vesper.utils.file_utils import create_dir


class Logger:
    instances = {}

    @staticmethod
    def get_logger(name, cmd_line):
        if name in Logger.instances:
            return Logger.instances[name].logger
        Logger.instances[name] = Logger(
            name, cmd_line.app, cmd_line.instance, cmd_line.logsdir)
        return Logger.instances[name].logger

    def __init__(self, name, app, instance, logsdir):
        self.name = name
        self.app = app
        self.instance = instance
        self.logsdir = logsdir
        self.logger = getLogger(self.name)
        self.logger.setLevel(DEBUG)
        self.set_format()

    def set_format(self):
        file = self.get_file()
        handler = FileHandler(filename=file)
        formatter = Formatter(
            fmt='%(asctime)s - %(levelname)s - %(module)s - %(lineno)d - %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    @staticmethod
    def create_logs_dir_path(logsdir, app, instance):
        date = datetime2str(fmt='%Y%m%d')
        logsdir = f'{logsdir}/{date}/{app.lower()}/{instance.lower()}'
        create_dir(logsdir)
        return logsdir

    def get_file(self):
        logsdir = Logger.create_logs_dir_path(
            self.logsdir, self.app, self.instance)
        return f'{logsdir}/{self.name}.log'

    @staticmethod
    def close_all_loggers():
        for name, instance in Logger.instances.items():
            instance.logger.info(f'closing logger instance [{name}]')
            for handler in instance.logger.handlers:
                handler.flush()
                handler.close()


class JsonFile:
    @staticmethod
    def write(file_name, elements):
        with open(file_name, 'a+') as outfile:
            for element in elements:
                json.dump(element, outfile, default=jsonutils.jdefault)
                outfile.write('\n')

    @staticmethod
    def read(file_name):
        instruments = []
        with open(file_name) as f:
            for line in f:
                instruments.append(json.loads(line))
        return instruments


class RefDataLogger:
    @staticmethod
    def get_refdata_file(app_logs_dir):
        return f'{app_logs_dir}/refdata.log'

    @staticmethod
    def write(elements):
        refdata_file = RefDataLogger.get_refdata_file(
            Logger.create_logs_dir_path())
        JsonFile.write(refdata_file, elements)


cmd_line = parser.parse_known_args()[0]
logger = None
try:
    logger = Logger.get_logger(cmd_line.app, cmd_line)
except:
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
