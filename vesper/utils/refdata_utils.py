from vesper.utils.loggers import Logger, RefDataLogger
from vesper.refdata.refdata import Refdata


def load_hist_refdata(hist_dir, hist_date, app, instance):
    hist_dir = Logger.get_app_logs_dir(hist_dir, hist_date, app, instance)
    refdata_file = RefDataLogger.get_refdata_file(hist_dir)
    Refdata.load_ref_data(refdata_file)
