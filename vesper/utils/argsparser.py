from argparse import ArgumentParser


def add_common_fields():
    parser = ArgumentParser()
    parser.add_argument('-a', '--app', required=False,
                        type=str, help="app name")
    parser.add_argument('-i', '--instance', required=False,
                        type=str, help="instance name")
    parser.add_argument('-l', '--logsdir', required=False,
                        type=str, help="logs dir")
    return parser


parser = add_common_fields()


class Cmd:
    parser = add_common_fields()

    @staticmethod
    def add_argument(*args, **kwargs):
        Cmd.parser.add_argument(*args, **kwargs)

    def __init__(self):
        pass

    @staticmethod
    def init_default_values(cmd):
        if cmd.app is None:
            cmd.app = 'default'

        if cmd.logsdir is None:
            cmd.logsdir = 'logs'

        if cmd.instance is None:
            cmd.instance = 'default'

    @staticmethod
    def parse():
        cmd = Cmd.parser.parse_known_args()[0]
        Cmd.init_default_values(cmd)
        return cmd
