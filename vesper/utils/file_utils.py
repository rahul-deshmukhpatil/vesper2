import os

import pandas as pd

from vesper.utils.exception import CannotReadWriteFile
from vesper.utils.geturl import get_url


def create_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

    if not os.path.exists(directory):
        raise CannotReadWriteFile(directory)


def write_file(target_file_path, file_content):
    out_file = open(target_file_path, 'wb')
    out_file.write(file_content)


def download_file(url, target_file_path):
    file_content = get_url(url)
    write_file(target_file_path, file_content)


def read_csv_as_pd_df(file_path, *args, **kwargs):
    return pd.read_csv(file_path, *args, **kwargs)


def validate_csv_file_rows(file_path, minimum_rows):
    return len(pd.read_csv(file_path)) >= minimum_rows


def validate_file_size(file_path, minimum_size):
    return os.path.getsize(file_path) >= minimum_size


def read_json_as_pd_df(file_path, *args, **kwargs):
    return pd.read_json(file_path, *args, **kwargs)


def write_as_hdf5(df, file, table):
    df.to_hdf(file, table)
