import json
import datetime
import enum

#https://stackoverflow.com/questions/6578986/how-to-convert-json-data-into-a-python-object
def _json_object_hook(d):
	return namedtuple('X', d.keys())(*d.values())

def json2obj(data):
	return json.loads(data, object_hook=_json_object_hook)

def custom_datetime_encoder(o):
  if type(o) is datetime.date or type(o) is datetime.datetime:
      return str(o)
  return None

#https://pythontips.com/2013/08/08/storing-and-loading-data-with-json/
def jdefault(o):
	if isinstance(o, datetime.date):
		return str(o)
	if isinstance(o,datetime.datetime):
		return str(o)
	elif isinstance(o, set):
		return list(o)
	elif isinstance(o, enum.Enum):
		return o.__str__()
	else:
		return o.__dict__


if __name__ == '__main__':
	pass
