import functools

from vesper.utils import exception
from vesper.utils.loggers import logger


def log_function_entry(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        reason = f'called {func})'
        logger.info(f'{reason}')
        func(*args, **kwargs)

    return wrapper


def log_exception(func, return_value):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except exception as ex:
            exception_str = f'unhandled exception, thrown in {func}, exception: {ex}'
            logger.exception(exception_str)
            raise ex

    return wrapper


def assert_over_fail(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        reason = f'parent class must override method {func}' \
                 f' of parent class {args[0].__class__.__name__}'
        logger.info(f'{reason}')
        print(f'{reason}')
        assert False

    return wrapper
