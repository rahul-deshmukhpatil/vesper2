import csv
from datetime import datetime

from vesper.utils.loggers import Logger
from vesper.strategy.strategy import Strategy, Leg
from vesper.types.types import Exchange, Side

logger = Logger.get_logger()
bn_stocks = ['AXISBANK', 'BANKBARODA', 'CANBK', 'FEDERALBNK', 'HDFCBANK',
             'ICICIBANK', 'RBLBANK', 'INDUSINDBK', 'KOTAKBANK', 'PNB', 'SBIN', 'BANDHANBNK']


class CustomLeg(Leg):
    def __init__(self, exchange, symbol, side):
        super(CustomLeg, self).__init__(exchange, symbol, side)

        self.ffm = None
        with open('/home/rahul/ffm.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                if row[0] == symbol:
                    self.ffm = int(row[1])

        if self.ffm is None:
            assert(False)


class SampleStrategy(Strategy):
    def __init__(self):
        super(SampleStrategy, self).__init__()
        for symbol in bn_stocks:
            leg = CustomLeg(Exchange.NSE, symbol, Side.BUY)
            self.add_leg(leg)

    def setup(self):
        pass

    def run(self):
        pass

    def on_data(self):
        #unit_price = 11114321495493 / 25001
        unit_price = 444555077
        cash_value = 0.0
        for leg in self.legs:
            cash_value += leg.mp() * leg.ffm
        bn_value = cash_value / unit_price
        timestamp = datetime.fromtimestamp(
            self.legs[0].md.epoch).strftime('%c')
        logger.info(f'Time:{timestamp} Value:{bn_value}')

    def close(self):
        pass
