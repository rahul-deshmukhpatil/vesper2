from vesper.types.instrument import Instrument

books = {}


def create_book(exchange, symbol):
    return Instrument(exchange, symbol)


def get_book(exchange, symbol):
    if exchange not in books:
        books[exchange] = {}

    if symbol not in books[exchange]:
        books[exchange][symbol] = create_book(exchange, symbol)

    return books[exchange][symbol]