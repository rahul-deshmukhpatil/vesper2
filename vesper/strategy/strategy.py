from datetime import datetime

from vesper.positions.positions import PositionManager
from vesper.refdata.symbols import Symbols
from vesper.strategy.books import get_book
from vesper.types.types import Side


class Symbol:
    def __init__(self, exchange, symbol):
        self.exchange = exchange
        self.symbol = symbol
        self.instrument_id = None
        self.md = get_book(exchange, symbol)

    def get_subscription(self):
        self.instrument_id = Symbols.all_symbols[self.exchange][self.symbol].instrument_id
        return self.instrument_id, self.exchange, self.symbol

    def bp(self):
        return self.md.bids[0].price

    def sp(self):
        return self.md.asks[0].price

    def mp(self):
        return (self.bp() + self.sp()) / 2


class Leg(Symbol):
    def __init__(self, exchange, symbol, side, ratio=1):
        super(Leg, self).__init__(exchange, symbol)
        self.side = side
        self.ratio = ratio

    def mkt_bp(self):
        return self.ratio * (self.sp() if self.side == Side.BUY else -self.bp())

    def mkt_sp(self):
        return self.ratio * (-self.bp() if self.side == Side.BUY else self.sp())

    def timestamp(self):
        return datetime.fromtimestamp(self.md.epoch).strftime('%c')


class Strategy(PositionManager):
    def __init__(self, strategies):
        self.strategies = strategies
        self.legs = []
        pass

    def setup(self):
        self.subscribe()

    def subscribe(self):
        instruments = self.get_subscriptions()
        self.strategies.subscribe(instruments)

    def get_subscriptions(self):
        subs = []
        for leg in self.legs:
            sub = leg.get_subscription()
            subs.append(sub)

        return subs

    def add_leg(self, leg):
        self.legs.append(leg)

    def mkt_bp(self):
        price = 0.0
        for leg in self.legs:
            price += leg.mkt_bp()
        return price

    def mkt_sp(self):
        price = 0.0
        for leg in self.legs:
            price += leg.mkt_sp()
        return price

    def on_data_updated(self):
        self.on_data()
        pass

    def mkt_price(self, side):
        if side == Side.BUY:
            return self.mkt_bp()
        else:
            return self.mkt_sp()

    def can_price(self):
        for leg in self.legs:
            if not leg.md:
                return False
        return True
