from vesper.date.date import epoch_to_timestamp
from vesper.mktdata.indicators import Indicator
from vesper.positions.positions import PositionManager
from vesper.utils.loggers import Logger
from vesper.strategy.strategy import Strategy, Leg
from vesper.types.types import Exchange, Side, Interval

logger = Logger.get_logger()


class EquityTa(Strategy):
    def __init__(self, strategies):
        Strategy.__init__(self, strategies)
        PositionManager.__init__(self, 10, 0)

        self.equity = Leg(Exchange.NSE, 'INFY', Side.BUY)
        self.add_leg(self.equity)
        self.sma20 = Indicator('SMA', Interval.MIN1, timeperiod=20)
        self.adx = Indicator('ADX', Interval.MIN1, timeperiod=20)

        self.pv = self.equity.md.pv

    def setup(self):
        Strategy.setup(self)
        pass

    def run(self):
        pass

    def on_data(self):
        if not self.can_price():
            return
        #sma20 = self.sma20.get(self.pv.df.close)
        adx = self.adx.get(self.pv.df.high, self.pv.df.low, self.pv.df.close)
        timestamp = epoch_to_timestamp(self.equity.md.epoch)
        logger.info(f'{timestamp} current_profit : ')

        self.modify_pricing_orders()

    def close(self):
        pass
