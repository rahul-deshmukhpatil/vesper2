from vesper.date.date import epoch_to_timestamp
from vesper.positions.positions import PositionManager
from vesper.utils.loggers import Logger
from vesper.strategy.strategy import Strategy, Leg
from vesper.types.types import Exchange, Side

logger = Logger.get_logger()


class BankNiftyEtfArb(Strategy):
    def __init__(self):
        Strategy.__init__(self)
        PositionManager.__init__(self, 10, 0)

        self.kotak_bk_etf = Leg(Exchange.NSE, 'KOTAKBKETF', Side.BUY)
        self.nippon_bk_etf = Leg(Exchange.NSE, 'BANKBEES', Side.SELL)

        self.add_leg(self.kotak_bk_etf)
        self.add_leg(self.nippon_bk_etf)

        self.kotak_nippon_ratio = 1.0045
        self.profitable_sell_ratio = 1.0075
        self.profitable_buy_ratio = 1.0035

    def setup(self):
        pass

    def run(self):
        pass

    def on_data(self):
        if not self.can_price():
            return

        current_buy_ratio = self.kotak_bk_etf.md.bp() / self.nippon_bk_etf.md.sp()
        current_sell_ratio = self.kotak_bk_etf.md.sp() / self.nippon_bk_etf.md.bp()

        if current_buy_ratio <= self.profitable_buy_ratio:
            self.buy(10)

        if current_sell_ratio >= self.profitable_sell_ratio:
            self.sell(10)

        timestamp = epoch_to_timestamp(self.kotak_bk_etf.md.epoch)
        logger.info(f'{timestamp} current_profit : {self.current_profit()}')

        self.modify_pricing_orders()

    def price_for_buying(self):
        self.kotak_fbp = self.nippon_bk_etf.md.bids[4].price * \
            self.profitable_buy_ratio
        self.nippon_fsp = self.kotak_bk_etf.md.asks[4].price / \
            self.profitable_buy_ratio
        pass

    def price_for_selling(self):
        self.kotak_fsp = self.nippon_bk_etf.md.asks[4].price * \
            self.profitable_sell_ratio
        self.nippon_fbp = self.kotak_bk_etf.md.bids[4].price / \
            self.profitable_sell_ratio
        pass

    def close(self):
        pass
