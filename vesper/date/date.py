from datetime import datetime

VESPER_DATE_FMT = '%Y%m%d'
VESPER_TIME_FMT = '%H:%M:%S'
VESPER_DATETIME_FMT = f'{VESPER_DATE_FMT}-{VESPER_TIME_FMT}'


def datetime2str(fmt, timestamp=None):
    timestamp = timestamp or datetime.now()
    return timestamp.strftime(fmt)


def date2str(fmt, timestamp=None):
    timestamp = timestamp or datetime.now()
    return timestamp.strftime(fmt)


def str2datetime(time_str, fmt):
    return datetime.strptime(time_str, fmt)


def epoch_to_timestamp(epoch):
    return datetime.fromtimestamp(epoch).strftime('%c')


def get_epoch(timestamp=None):
    timestamp = timestamp or datetime.now()
    return timestamp.timestamp()


def get_int_epoch(timestamp=None):
    return int(get_epoch(timestamp))
