class DemoClass:
    def __init__(self):
        self.data = 1

    def demo_method(self):
        return f'ran actual method for self.data:{self.data}'


demo_object = DemoClass()
