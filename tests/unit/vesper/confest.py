from os import path

from vesper.date.date import get_epoch
from vesper.utils.file_utils import create_dir


def random_dir():
    dir_path = path.join('/tmp', f'{get_epoch()}')
    create_dir(dir_path)
    return dir_path
