from os import path
from unittest.mock import Mock, patch

import pytest

from tests.unit.vesper.confest import random_dir
from vesper.connectors.common.symbols import NewSymbols
from vesper.connectors.refdata.nse.api import NseRefdataApi

random_dir = random_dir()

mock_arguments = Mock()
mock_arguments.app = 'refdata'
mock_arguments.instance = 'nse'
mock_arguments.logsdir = path.join(random_dir, 'logs')
mock_arguments.histdir = path.join(random_dir, 'db')
mock_arguments = [mock_arguments, ]

symbols_mock_arguments = Mock()
symbols_mock_arguments.app = 'symbols'
symbols_mock_arguments.instance = 'nse'
symbols_mock_arguments.logsdir = path.join(random_dir, 'logs')
symbols_mock_arguments.histdir = path.join(random_dir, 'db')
symbols_mock_arguments = [symbols_mock_arguments, ]


class TestSymbols:
    @pytest.mark.parametrize('index_name, min_underlyings',
                             (('nifty50', 50),
                              ('nifty100', 100),
                              ('nifty200', 200),
                              ('nifty500', 500)))
    @patch('vesper.connectors.refdata.api.parser')
    @patch('vesper.connectors.common.symbols.parser')
    def test_nifty500_symbols(self, mock_symbols_parser, mock_parser,
                              index_name,
                              min_underlyings):
        mock_parser.parse_known_args.return_value = mock_arguments
        nse_app = NseRefdataApi()
        nse_app.setup()
        nse_app.download_files()

        mock_symbols_parser.parse_known_args.return_value = symbols_mock_arguments
        symbols = NewSymbols()
        index_symbols = symbols.get_nifty_symbols(index_name)
        assert (len(index_symbols) >= min_underlyings)

    @pytest.mark.parametrize('constituents',
                             (dict([('nifty50', 50),
                              ('nifty100', 100),
                              ('nifty200', 200),
                              ('nifty500', 500)]),))
    @patch('vesper.connectors.refdata.api.parser')
    @patch('vesper.connectors.common.symbols.parser')
    def test_nifty500_symbols(self, mock_symbols_parser, mock_parser,
                              constituents):
        mock_parser.parse_known_args.return_value = mock_arguments
        nse_app = NseRefdataApi()
        nse_app.setup()
        nse_app.download_files()

        mock_symbols_parser.parse_known_args.return_value = symbols_mock_arguments
        symbols = NewSymbols()
        for index_name, min_constituents in constituents.values():
            index_symbols = symbols.get_nifty_symbols(index_name)
            assert (len(index_symbols) >= min_constituents)
