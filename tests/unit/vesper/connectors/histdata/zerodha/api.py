from os import path
from unittest.mock import Mock, patch

import pytest

from tests.unit.vesper.confest import random_dir
from vesper.connectors.histmktdata.zerodha.api import ZerodhaHistMktdataApi

random_dir = random_dir()

mock_arguments = Mock()
mock_arguments.app = 'histdata'
mock_arguments.instance = 'nse'
mock_arguments.from_date = '20160120'
mock_arguments.to_date = '20220220'
mock_arguments.logsdir = path.join(random_dir, 'logs')
mock_arguments.histdir = path.join(random_dir, 'db')
mock_arguments = [mock_arguments, ]


class TestZerodhaMktdataApi:
    @patch('vesper.connectors.histmktdata.api.parser')
    def test_zerodha_hist_data_api(self, mock_parser):
        mock_parser.parse_known_args.return_value = mock_arguments
        zapi = ZerodhaHistMktdataApi()
        zapi.setup()
        zapi.run()
