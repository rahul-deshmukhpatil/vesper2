from os import path
from unittest.mock import Mock, patch

import pytest

from tests.unit.vesper.confest import random_dir
from vesper.connectors.common.symbols import NewSymbols
from vesper.connectors.refdata.nse.api import NseRefdataApi

random_dir = random_dir()

mock_arguments = Mock()
mock_arguments.app = 'refdata'
mock_arguments.instance = 'nse'
mock_arguments.logsdir = path.join(random_dir, 'logs')
mock_arguments.histdir = path.join(random_dir, 'db')
mock_arguments = [mock_arguments, ]

symbols_mock_arguments = Mock()
symbols_mock_arguments.app = 'symbols'
symbols_mock_arguments.instance = 'nse'
symbols_mock_arguments.logsdir = path.join(random_dir, 'logs')
symbols_mock_arguments.histdir = path.join(random_dir, 'db')
symbols_mock_arguments = [symbols_mock_arguments, ]


class TestZerodhaMktdataApi:
    @patch('vesper.connectors.refdata.api.parser')
    @patch('vesper.connectors.common.symbols.parser')
    def test_nifty500_symbols(self, mock_symbols_parser, mock_parser):
        mock_parser.parse_known_args.return_value = mock_arguments
        nse_app = NseRefdataApi()
        nse_app.setup()
        nse_app.download_files()

        mock_symbols_parser.parse_known_args.return_value = symbols_mock_arguments
        symbols = NewSymbols()
