import unittest
from os import path

import numpy as np
import pandas as pd
import pandas.testing as pd_testing

from tests.unit.vesper.confest import random_dir
from vesper.utils.file_utils import read_csv_as_pd_df


class TestFileUtils(unittest.TestCase):
    def test_read_csv_as_pd_df(self):
        random_file = path.join(random_dir(), 'sample.csv')
        expected_df = pd.DataFrame(np.random.randn(6, 4), columns=list("ABCD"))
        expected_df.to_csv(random_file, index=False)
        read_df = read_csv_as_pd_df(random_file)
        pd_testing.assert_frame_equal(expected_df, read_df)
