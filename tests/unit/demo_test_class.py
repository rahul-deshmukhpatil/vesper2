import unittest
from os import path
from unittest.mock import Mock, patch
from tests.unit.vesper.utils.file_utils import random_dir
from vesper.connectors.refdata.nse.api import NseRefdataApi

mock_arguments = Mock()
mock_arguments.app = 'test_app'
mock_arguments.instance = 'test_instance'
random_dir = random_dir()
mock_arguments.logsdir = path.join(random_dir + 'logs')
mock_arguments.histdir = path.join(random_dir + 'db')
mock_arguments = [mock_arguments, ]


class TestDemoClass(unittest.TestCase):
    @patch('vesper.connectors.refdata.api.parser')
    def test_demo_method(self, mock_parser):
        mock_parser.parse_known_args.return_value = mock_arguments
        nse_app = NseRefdataApi()
        assert nse_app.cmd_line == 'test_app'
