import unittest
from unittest.mock import Mock

from requests import Timeout

requests = Mock()


def get_response(url):
    r = requests.my_get_url(url)
    if r.code == 200:
        return r.json()
    return None


class TestMockFunc(unittest.TestCase):
    def log_get_response(self, url):
        print(f'called get_response : {url}')
        ret = Mock()
        ret.code = 200
        ret.json.return_value = {'a': 1, 1: 'a'}
        return ret

    def test_get_response(self):
        requests.my_get_url.side_effect = Timeout
        with self.assertRaises(Timeout):
            get_response('dummy_url')

    def test_log_get_response(self):
        requests.my_get_url.side_effect = self.log_get_response
        assert {'a': 1, 1: 'a'} == get_response('dummy_url_to_log_response')
        assert requests.my_get_url.call_count in [1, 4]

    def test_configure_get_response(self):
        requests.my_get_url.configure_mock(side_effect=[Timeout,
                                                        self.log_get_response])
        with self.assertRaises(Timeout):
            get_response('dummy_url')
        requests.my_get_url.side_effect = self.log_get_response
        assert {'a': 1, 1: 'a'} == get_response('dummy_url_to_log_response')
        assert requests.my_get_url.call_count == 2
