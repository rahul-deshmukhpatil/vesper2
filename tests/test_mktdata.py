import unittest

from vesper.apps.old_mktdata import Mktdata


class TestMktdata(unittest.TestCase):
    def test_mktdata(self):
        mktdata = Mktdata()
        mktdata.setup()
        zerodha = mktdata.zerodha_mktdata.zerodha
        self.assertEqual(zerodha.loggedIn, True)
        #mktdata.run()


if __name__ == '__main__':
    unittest.main()
