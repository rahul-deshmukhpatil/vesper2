cd ~/bitbucket/vesper2
DATE=`date`
echo "$DATE $PWD: running vesper" >> ~/sample.crontab
export DISPLAY=:0

source scripts/pre_start.sh
nohup /usr/bin/python3 -m vesper.apps.mktdata -a mktdata -i mktdata -l /home/rahul/bitbucket/temp &
echo "$DATE $PWD: started vesper" >> ~/sample.crontab
