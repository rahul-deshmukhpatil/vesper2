
from vesper.refdata.expiry import Expiry
from vesper.types.types import Exchange
from datetime import date
import unittest


class TestExpiry(unittest.TestCase):
    def test_expiry(self):
        expiries = Expiry.get_expiries(Exchange.NFO, date.fromisoformat('2020-05-15'), 3)
        self.assertEqual(expiries, [
            date.fromisoformat('2020-05-28'),
            date.fromisoformat('2020-06-25'),
            date.fromisoformat('2020-07-30'),
        ])

        expiries = Expiry.get_weekly_expiries(Exchange.NFO, date.fromisoformat('2019-05-16'), 3)
        self.assertEqual(expiries, [
            date.fromisoformat('2019-05-16'),
            date.fromisoformat('2019-05-23'),
            date.fromisoformat('2019-05-30'),
        ])

        expiries = Expiry.get_expiries(Exchange.CDS, date.fromisoformat('2020-05-14'), 3)
        self.assertEqual(expiries, [
            date.fromisoformat('2020-05-27'),
            date.fromisoformat('2020-06-26'),
            date.fromisoformat('2020-07-29'),
        ])

        expiries = Expiry.get_weekly_expiries(Exchange.CDS, date.fromisoformat('2019-05-17'), 3)
        self.assertEqual(expiries, [
            date.fromisoformat('2019-05-17'),
            date.fromisoformat('2019-05-24'),
            date.fromisoformat('2019-05-31'),
        ])


if __name__ == '__main__':
    unittest.main()
