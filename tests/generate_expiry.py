from datetime import date
from datetime import timedelta

import unittest


class TestGenerateExpiry(unittest.TestCase):
    @staticmethod
    def print_expiry(from_date, till_date, weekday):
        while from_date != till_date:
            if from_date.weekday() == weekday:
                print(f'get_date(\'{from_date.day}-{from_date.month}-{from_date.year}\')')
            from_date = from_date + timedelta(days=+1)

    def test_generate_expiry(self):
        year=2020
        from_date = date.fromisoformat(f'{year}-01-01')
        till_date = date.fromisoformat(f'{year+1}-01-01')

        TestGenerateExpiry.print_expiry(from_date, till_date, 3)
        TestGenerateExpiry.print_expiry(from_date, till_date, 4)


if __name__ == '__main__':
    unittest.main()
