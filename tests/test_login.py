import unittest

from vesper.zerodha.connect import Zerodha


class TestLogin(unittest.TestCase):
    def test_login(self):
        zerodha = Zerodha()
        zerodha.login_zerodha()
        self.assertEqual(zerodha.loggedIn, True)


if __name__ == '__main__':
    unittest.main()
