# README #

Vesper is quant strategies backtesting and research platform.

Much of the work is inspired from the backtrader but needed custom functionality so could not use the backtrader.
It uses lot of open source projects mainly

Kafka
ta-lib
pandas
AHL Mans arctict

### What is this repository for? ###

* vesper  
* 0.9 beta 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
	Auto setup is yet not supported. You need to install the packages specified in the scripts/install.sh
* Configuration
	No configuration is needed. Every python app has a command line parameters for it
* Dependencies
	Mainly 4, Kafka, Talib, Pandas, AHL Mans arctict
* Database configuration
	Mongodb is used by the AHL Mans arctict
* How to run tests
	tests are supported under tests dir, you may directly run them
* Deployment instructions
	deployed as source. place under ~/bitbucket dir to reuse the start/stop scripts

### Contribution guidelines ###

This is personal project under beta version
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin 
	rahul.deshmukhpatil@gmail.com
* Other community or team contact
