from vesper.utils.kafka_prod_cons import KafkaMessanger

broker = 'ZERODHA'
kafka_messanger = KafkaMessanger(cons_topics=[broker, ])
kafka_messanger.setup()
kafka_messanger.publish_kafka_message(broker, "{hi: 1}")
