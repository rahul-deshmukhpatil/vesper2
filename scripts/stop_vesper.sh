#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

echo "dir : $dir"
"$SCRIPT_DIR"/vesper_status.sh

echo "stopping vesper" >> ~/sample.crontab
vesper_pid=`pgrep -f vesper.apps.mktdata`
kill -2 $vesper_pid

# twice because fyers mktdata keeps running
sleep 2
vesper_pid=`pgrep -f vesper.apps.mktdata`
kill -2 $vesper_pid

sleep 2
echo "stopping vesper" >> ~/sample.crontab
vesper_pid=`pgrep -f vesper.apps.feed_logger`
kill -2 $vesper_pid

sleep 2
"$SCRIPT_DIR"/vesper_status.sh

