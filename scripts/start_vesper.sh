#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DATE=$(date "+%Y%m%d")

prod_or_test=$1
# shellcheck disable=SC2252
if [[ $prod_or_test != "test" && $prod_or_test != "prod" ]];
then
  echo "Run as prod or test"
  exit 255
fi

#export the variables and start kafka
#"$SCRIPT_DIR"/pre_start.sh

base_dir="/home/rahul/bitbucket/$prod_or_test"
logsdir="$base_dir/logs"
histdir="$base_dir/db"

echo "$DATE $PWD: running vesper" >> ~/sample.crontab
export DISPLAY=:0

function start_app()
{
  app=$1
  instance=$2
  app_stdout="$logsdir/$DATE/$app/$instance/$app.std.out.err"
  mkdir -p $(dirname "$app_stdout")
  #start the raw listener feed
  python3 -m vesper.apps.$app -a $app -i $instance -l $logsdir -d $histdir > $app_stdout 2>&1
}

function nohup_start_app()
{
  app=$1
  instance=$2
  app_stdout="$logsdir/$DATE/$app/$instance/$app.std.out.err"
  mkdir -p $(dirname "$app_stdout")
  #start the raw listener feed
  nohup python3 -m vesper.apps.$app -a $app -i $instance -l $logsdir -d $histdir > $app_stdout 2>&1 &
}


#start the raw listener feed
start_app refdata nse
start_app refdata fyers
start_app refdata zerodha


NOHUP='nohup bash -c'
nohup_start_app feed_logger mktdata &
sleep 2
nohup_start_app mktdata fyers &
nohup_start_app mktdata zerodha &
