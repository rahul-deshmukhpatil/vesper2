cd ~/bitbucket/vesper2
DATE=`date`
export DISPLAY=:0

source scripts/pre_start.sh

app=$1
shift 1
python3 -m vesper.apps.$app -a $app -i $app -l /home/rahul/bitbucket/temp/ $@
