app=$1
echo "stopping vesper app: $app"
app_pid=`pgrep -f vesper.apps.$app`
kill -2 $app_pid
