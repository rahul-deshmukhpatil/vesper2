#!/bin/bash

#start zookeeper
#service zookeeper start

# start kafka
KAKFA_VERSION=kafka_2.13-3.1.0
KAKFA_VERSION=kafka_2.13-2.6.0
nohup /home/rahul/bitbucket/kafka/$KAKFA_VERSION/bin/kafka-server-start.sh  /home/rahul/bitbucket/kafka/$KAKFA_VERSION/config/server.properties 2>&1 > kafka.log &

KAFKA_ADDRESS="localhost:2181"

if [[ $KAFKA_ADDRESS == '' ]];
then
  exit 255
fi

/home/rahul/bitbucket/kafka/$KAKFA_VERSION/bin/kafka-topics.sh --zookeeper $KAFKA_ADDRESS --replication-factor 1 --partitions 1 --create --topic "raw-feed"
/home/rahul/bitbucket/kafka/$KAKFA_VERSION/bin/kafka-topics.sh --zookeeper $KAFKA_ADDRESS --replication-factor 1 --partitions 1 --create --topic "test-raw-feed"
/home/rahul/bitbucket/kafka/$KAKFA_VERSION/bin/kafka-topics.sh --zookeeper $KAFKA_ADDRESS --replication-factor 1 --partitions 1 --create --topic "INVALID_BROKER"
/home/rahul/bitbucket/kafka/$KAKFA_VERSION/bin/kafka-topics.sh --zookeeper $KAFKA_ADDRESS --replication-factor 1 --partitions 1 --create --topic "KOTAK"
/home/rahul/bitbucket/kafka/$KAKFA_VERSION/bin/kafka-topics.sh --zookeeper $KAFKA_ADDRESS --replication-factor 1 --partitions 1 --create --topic "ZERODHA"
/home/rahul/bitbucket/kafka/$KAKFA_VERSION/bin/kafka-topics.sh --zookeeper $KAFKA_ADDRESS --replication-factor 1 --partitions 1 --create --topic "FYERS"
/home/rahul/bitbucket/kafka/$KAKFA_VERSION/bin/kafka-topics.sh --zookeeper $KAFKA_ADDRESS --replication-factor 1 --partitions 1 --create --topic "NSE"
